	//var urls = ["https://www.purvidoshi.com/shopify-multi-gallery-slider/zestard/assets/js/prototype.js","https://www.purvidoshi.com/shopify-multi-gallery-slider/zestard/assets/js/jquery.gallery.js","https://www.purvidoshi.com/shopify-multi-gallery-slider/zestard/assets/js/jquery.flexslider.js"];
    var urls = ["https://www.zestardshop.com/shopifyapp/multipageslider/zestard/assets/js/jquery.flexslider.js"];
    for (var i = 0; i < urls.length; i++) {
      var s = document.createElement('script');
      s.type = 'text/javascript';
      s.async = false;
      s.src = urls[i];
      var x = document.getElementsByTagName('script')[0];
      x.parentNode.insertBefore(s, x);
    }    

  window.attachEvent ? window.attachEvent('onload', asyncLoad) : window.addEventListener('load', asyncLoad, false);

  	function asyncLoad() {
  		
	    jQuery('.slider').each(function(){
	    	var slideTime = jQuery(this).find('.cat_slide_time').val();
	    	jQuery(this).find('.flexslider').flexslider({
				animation: "slide",
				slideshowSpeed: slideTime,				
				start: function(slider)
				{
				  jQuery('body').removeClass('loading');
				}
			});			
  		});   
	    

	    jQuery('.cslide li').click(function()
	     {
	        var val = parseInt(jQuery(this).index()+1);
	        jQuery(this).parent().parent().find('.flex-control-nav li:nth-child('+val+') a').click();

	    });	   	
	}
 (function(){
	  // Your base, I'm in it!
	  var originalAddClassMethod = jQuery.fn.addClass;
		jQuery.fn.addClass = function()
		{
		  // Execute the original method.
		  var result = originalAddClassMethod.apply( this, arguments );
		  // call your function
		  // this gets called everytime you use the addClass method
		  window.checkFunction(result);
		  // return the original result
		  return result;
		}
	})();

function checkFunction(result)
{
	if(result.hasClass('flex-active'))
	{
		var indexNumber = parseInt(result.parent().index())+1;
		jQuery(result).parents('.flexslider').find('.cslide li a').removeClass('flex-active');
		jQuery(result).parents('.flexslider').find('.cslide li:nth-child('+indexNumber+')').find('a').attr('class','flex-active');
	}
}

loadCSS = function(href) {			
	var cssLink = $("<link rel='stylesheet' type='text/css' href='"+href+"'>");
	jQuery("head").append(cssLink); 
};
loadCSS("https://www.zestardshop.com/shopifyapp/multipageslider/zestard/assets/css/flexslider.css");
loadCSS("https://www.zestardshop.com/shopifyapp/multipageslider/zestard/assets/css/custom.css");

jQuery(document).ready(function() {
	var ids = jQuery('.zestard-multislider').map(function(){
    	return this.id
	}).get();
	
	jQuery.ajax({
		url:'https://www.zestardshop.com/shopifyapp/multipageslider/zestard/check?ids='+ids+'&shop='+document.domain,
		dataType: "json",
		method: "get",
		success:function(res){			
			var count = 0;
            jQuery(".zestard-multislider").each(function(){
            	var that = this;
            	jQuery(this).html(res[count]);
            	count++; 
            });
            asyncLoad();			
		},
		error: function(xhr, status, err) {
			jQuery("<p>Error: Status = " + status + ", err = " + err + "</p>")
			.appendTo(document.body);
		}
	});			
});