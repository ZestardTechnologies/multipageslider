<div id="wrap">	
	<div class="col-md-8 col-xs-12">
		<h1 class="info-head">How to Use ?</h1>
		<div class="alert alert-info custom-fonts" style="background: #d3dbe2; color: #494949; border: 1px solid #ccc">						
			<div>
				1) First go to <a href="<?php echo base_url('/category'); ?>">"Manage Slider"</a> section and click on <a href="<?php echo base_url('/category/add'); ?>">"Add Slider"</a> button.
					<ul>
						<li>Then set Title, Slider Style, and Status (must be enabled)</li>  
						<li>You can also manage slider time (In milliseconds) of each slider differently, Example: 15000 milliseconds equal to 15 seconds.</i>
						<li>You can customize the slider style by entering CSS and class name of the slider.</i>
						<li>Then click on the save button.</li>
					</ul>
			</div>
			<div>
				2) After creating slider, Go to <a href="<?php echo base_url('/item'); ?>">"Manage Slides"</a> section click on <a href="<?php echo base_url('/item/add'); ?>">"Add slide"</a> button.<br/>
				   <ul>
						<li>Then set all the data according to your information</li>  
						<li>Image is required, silder must be chosen(In this merchant have to select slider, so that particular slide will show on particular silder).</i>
						<li>If you don't want to display sildes in any slider then select Uncategorized.</li>
						<li>You can add multiple slides on same Slider.</li>
						<li>You can enable / disable 'shop now' title and 'shop now' button on each slider.</li>
						<li>You can also set target window of 'shop now button' on each slider.</li>
						<li>You can add display order for every sildes in particular silder.</li>
					</ul>
			</div>
			<div>
				3) After creating slides, Now go to the <a href="<?php echo base_url('/category'); ?>">"Manage Slider"</a> section click on "Edit" link of your slider.<br/> 
				   	<ul>
						<li>You can see the label like "Paste this shortcode in any page or post"</li>  
						<li>Just click on the "Copied" button then copy the whole short HTML code.</i>
						<li>After copying, you can paste it in any page according to your requirement.</li>
						<li>Now you can see the slider in your site's front.</li>
					</ul>
			</div>		
		</div>		
	</div>
	<div class="col-md-4 col-xs-12">
		<div>
			<h1 class="info-head">Development Center</h1>
			<a href="http://www.zestard.com/" target="_blank" class="development-center">
				<img src="<?php echo base_url('/assets/images/zestard-logo.png') ?>" alt="Zestard"/>
			</a>
			<div class="alert alert-info" style="background: #d3dbe2; color: #494949; border: 1px solid #ccc">			
				<p><strong style="float:left">Email: </strong><a href="mailto:shopify@zestard.com">&nbsp;support@zestard.com</a></p>			
			</div>
		</div>
		<div>
			<h1 class="info-head">Benefits</h1>
			<div class="alert alert-info" style="background: #d3dbe2; color: #494949; border: 1px solid #ccc">		
				<ul class="benefits">
					<li>Easy To Use</li>
					<li>Easy to search and filter Slider and Slides</li>
					<li>Admin can create slider for single page as well as multiple sliders for multiple pages</li>
					<li>Responsive (Mobile / Tablet Friendly) Sliders</li>
					<li>Admin can have option to set slider style differently for each slider</li>
					<li>Slider automatically generates short code and Admin can paste it wherever it needs to be shown</li>
				</ul>				
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function copyToClipboard() {
        var st = document.getElementById("shortcode").value;
        window.prompt("Copy to clipboard: Ctrl+C, Enter", st);}
</script>
