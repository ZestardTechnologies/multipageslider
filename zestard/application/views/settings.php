<div id="wrap">		
	<div class="col-md-9 col-xs-12">
		<h1 style="font-size: 24px;">Manage Slider Configuration</h1>		
		<?php echo form_open('Settings/add',array('class' => 'custom-form-design', 'style' => 'border: 1px solid #ccc;padding: 20px;')); ?>
			<?php $getStoreCollection = $this->Slider_Settings_Model->getStoreIdByStoreName($_SESSION['shop']); ?>		    	
	      	<?php echo form_hidden('store_id',$getStoreCollection[0]->id); ?>			
			<?php 
			if($records != NULL)
				echo form_hidden('id',$records[0]->id);  			
			?>		
			<?php		    
				$options = array(
			      '0'  => 'Disable',
			      '1'  => 'Enable'      
			    );
			    $target_window = array(
			      '1'  => 'Same Window',
			      '2'  => 'New Window'      
			    );			    
			?>
			<fieldset>
				<div class="form-group">
					<?php echo form_label('Slide Time'); ?>					
					<div class="controls">
						<?php echo form_input(array('id'=>'slide_time','name'=>'slide_time','class'=>'form-control','value'=>($records == NULL ? '' : $records[0]->slide_time),'placeholder'=>'Enter Slide Time'));?>						
					</div>
				</div>
				<div class="form-group">
					<?php echo form_label('Show Shopnow'); ?>					
					<div class="controls">						
						<?php echo form_dropdown('show_shopnow',$options,($records != NULL) ? ($records[0]->show_shopnow == 0 ? 0 : 1) : ''); ?>
						<em>By default option is Disable</em>
					</div>						
				</div>
				<div class="form-group">
					<?php echo form_label('Show Title'); ?>					
					<div class="controls">						
						<?php echo form_dropdown('show_title',$options,($records != NULL) ? ($records[0]->show_title == 0 ? 0 : 1) : ''); ?>
						<em>By default option is Disable</em>
					</div>						
				</div>
				<div class="form-group">
					<?php echo form_label('Target Window'); ?>					
					<div class="controls">
						<?php echo form_dropdown('target_window',$target_window,($records != NULL) ?($records[0]->target_window == 1 ? 1 : 2) : '', 'style="width: 140px;'); ?>						
						<em>By default option is same window</em>
					</div>
				</div>
				<div style="margin-top: 20px;">
					<?php echo form_submit(array('id'=>'submit','value'=>'Save Changes','class'=>'btn btn-success'));  ?>					
				</div>				
			</fieldset>
		<?php echo form_close(); ?>
	</div>
</div>