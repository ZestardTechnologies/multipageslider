<div id="wrap">		
	<div id="item_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
		<div class="col-md-9 col-xs-12" style="padding-top: 10px">
			<div class="btn-set" style="width: 100%;margin-bottom: 10px;display: inline-flex;">
				<h1 style="font-size: 24px;width: 100%;">Manage Slides</h1>
				<a type="button" href="<?php echo base_url('item/add/'); ?>" value="Add Item" class="btn btn-primary" style="float: right;width: 15%;display: block;vertical-align: middle;padding: 16px;">Add Item</a>			
			</div>		
			<?php /*if($this->session->flashdata('item') != NULL) : ?>
				<?php $message = $this->session->flashdata('item'); ?>
				<div class="alert alert-danger"><?php //echo $message['message']; ?></div>
			<?php endif;*/ ?>		
			<?php if($records == NULL) : ?>
				<div class="alert alert-info" style="background: #f5f5f5; color: #494949; border: 1px solid #ccc">
			        There are no any gallery items.
			    </div>
			<?php else: ?>	

			<?php /* if($this->session->flashdata('msgSuccess')){ ?>
			<div class="alert alert-success">
   				<?php echo $this->session->flashdata('msgSuccess'); ?>
			</div>
			<?php } ?>

			<?php if($this->session->flashdata('msgFail')){ ?>
			<div class="alert alert-danger">
   				<?php echo $this->session->flashdata('msgFail'); ?>
			</div>
			<?php } */ ?>				
				<table id="item" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%" role="grid" aria-describedby="item_info" style="width: 100%;">
			        <thead>
			            <tr role="row">
			                <th class="sorting_desc" tabindex="0" aria-controls="item" rowspan="1" colspan="1" aria-label="ID: activate to sort column ascending" aria-sort="descending" style="width: 10px;">ID</th>
			                <th class="sorting" tabindex="0" aria-controls="item" rowspan="1" colspan="1" aria-label="Title: activate to sort column ascending" style="width: 397px;">Title</th>
			                <th class="sorting" tabindex="0" aria-controls="item" rowspan="1" colspan="1" aria-label="Category Name: activate to sort column ascending" style="width: 120px;">Category Name</th>			                			                
			                <th class="sorting" tabindex="0" aria-controls="item" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending" style="width: 50px;">Status</th>
			                <th class="sorting" tabindex="0" aria-controls="item" rowspan="1" colspan="1" aria-label="Display order: Order for display item on website" style="width: 50px;">Display order</th>
                                        <th class="sorting action-class" style="width: 70px;">Action</th>
			            </tr>
			        </thead>			        
			        <tbody>
			        	<?php $count = 1; ?>
			        	<?php foreach($records as $itemData) : ?>
						<?php 			
						$class = 'odd'; 
			        		if($count%2 == 0){
			        			$class = 'even';
			        		}			        	
							$getCategoryTitle = $this->Slider_Image_Items_Model->getCategoryTitleById($itemData->category_id);						
						?>						
						<tr role="row" class="<?php echo $class; ?>">
			                <td class="sorting_1">
			                    <?php echo $count++;; ?>
			                </td>
			                <td>
			                    <?php echo $itemData->title; ?>
			                </td>
			                <td>
			                    <?php echo ($getCategoryTitle == NULL) ? 'Uncategorized' : $getCategoryTitle[0]->title; ?>
			                </td>			                
			                <td>
			                    <?php echo ($itemData->status == 0 ? 'Disable' : 'Enable'); ?>
			                </td>
                                        <td>
			                    <?php echo ($itemData->display_order == NULL ? '' : $itemData->display_order); ?>
			                </td>
			                <td>
								<a href="<?php echo base_url('item/edit/'.$itemData->id); ?>">Edit</a> || <a onclick="return confirm('Are you sure you want to delete this item?')" href="<?php echo base_url('item/delete/'.$itemData->id); ?>">Delete</a>
			        		</td>
			        	</tr>	            
			        	<?php endforeach; ?>			        		
			        </tbody>
			    </table>
			<?php endif; ?>
		</div>
	</div>
</div>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('#item').DataTable();
	} );
</script>