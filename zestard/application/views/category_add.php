<div id="wrap">		
	<div class="col-md-9 col-xs-12">
		<h1 style="font-size: 24px;">Slider Form</h1>
		<div class="custom-form-design">
			<?php echo form_open('Category/category_add'); ?>
			<?php		    
				$options = array(
			      '0'  => 'Disable',
			      '1'  => 'Enable'      
			    );
			    $slider_style = array(
			      'slider1'  => 'Slider1',
			      'slider2'  => 'Slider2',      
			      'slider3'	 => 'Slider3',
			      'slider4'	 => 'Slider4'
			    );			  
			?>	    	
		    <fieldset>
		    	<?php $getStoreCollection = $this->Slider_Category_Model->getStoreIdByStoreName($this->session->userdata['shop']); ?>		    	
		      	<?php echo form_hidden('store_id',$getStoreCollection[0]->id); ?>  	
				<div class="form-group">
					<?php echo form_label('Title'); ?>
					<div class="controls">
						<?php echo form_input(array('id'=>'title','name'=>'title','class'=>'form-control'));  ?>
					</div>
				</div>
				<div class="form-group">
					<?php echo form_label('Slider Style'); ?>						
					<div class="controls">
						<?php echo form_dropdown('slider_style',$slider_style); ?>
						<div class="slider-demo">
							<p><strong>Diffrent Slider Styles:</strong> (Choose anyone of them)</p>
							<div class="slider" image-src="http://zestardshop.com//shopifyapp/multipageslider/zestard/assets/images/slider1.png">Slider1 Style Demo</div>
							<div class="slider" image-src="http://zestardshop.com//shopifyapp/multipageslider/zestard/assets/images/slider2.png">Slider2 Style Demo</div>
							<div class="slider" image-src="http://zestardshop.com//shopifyapp/multipageslider/zestard/assets/images/slider3.png">Slider3 Style Demo</div>
							<div class="slider" image-src="http://zestardshop.com//shopifyapp/multipageslider/zestard/assets/images/slider4.png">Slider4 Style Demo</div>
						</div>
					</div>
				</div>
				<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">              
							<div class="modal-body">
								<button type="button" class="close" data-dismiss="modal">
									<span aria-hidden="true">&times;</span>
									<span class="sr-only">Close</span>
								</button>
								<img src="" class="imagepreview" style="width: 100%;" >
							</div>
						</div>
					</div>
				</div>
				<script>
				jQuery(function() {
					jQuery('.slider').on('click', function() {
						jQuery('.imagepreview').attr('src', jQuery(this).attr('image-src'));
						jQuery('#imagemodal').modal('show');   
					});		
				});
				</script>
				<div class="form-group">
					<?php echo form_label('Slide Time <span>(Set the speed of the slideshow cycling, <strong>in milliseconds</strong>.)</span>'); ?>
					<div class="controls">
						<?php echo form_input(array('id'=>'catgory_slide_time','name'=>'catgory_slide_time','class'=>'form-control'));  ?>
					</div>
				</div>
				<div class="form-group">
					<?php echo form_label('Class Name'); ?>
					<div class="controls">
						<?php echo form_input(array('id'=>'catgory_class_name','name'=>'catgory_class_name','class'=>'form-control'));  ?>
					</div>
				</div>
				<div class="form-group">
					<?php echo form_label('Internal CSS <span>(In this box you can write your own CSS, For Example: <strong>.body {color: #ccccc;}</strong>)<span>'); ?>
					<div class="controls">						
						<textarea id="catgory_inline_css" rows="6" cols="10" class="form-control" name="catgory_inline_css"></textarea>
					</div>					
				</div>
				<div class="form-group">
					<?php echo form_label('Content Effect'); ?>						
					<div class="controls">
						<select name="item_effects" style="width: 200px;">
					        <optgroup label="Attention Seekers">
					          <option value="bounce">bounce</option>
					          <option value="flash">flash</option>
					          <option value="pulse">pulse</option>
					          <option value="rubberBand">rubberBand</option>
					          <option value="shake">shake</option>
					          <option value="swing">swing</option>
					          <option value="tada">tada</option>
					          <option value="wobble">wobble</option>
					          <option value="jello">jello</option>
					        </optgroup>

					        <optgroup label="Bouncing Entrances">
					          <option value="bounceIn">bounceIn</option>
					          <option value="bounceInDown">bounceInDown</option>
					          <option value="bounceInLeft">bounceInLeft</option>
					          <option value="bounceInRight">bounceInRight</option>
					          <option value="bounceInUp">bounceInUp</option>
					        </optgroup>

					        <optgroup label="Bouncing Exits">
					          <option value="bounceOut">bounceOut</option>
					          <option value="bounceOutDown">bounceOutDown</option>
					          <option value="bounceOutLeft">bounceOutLeft</option>
					          <option value="bounceOutRight">bounceOutRight</option>
					          <option value="bounceOutUp">bounceOutUp</option>
					        </optgroup>

					        <optgroup label="Fading Entrances">
					          <option value="fadeIn">fadeIn</option>
					          <option value="fadeInDown">fadeInDown</option>
					          <option value="fadeInDownBig">fadeInDownBig</option>
					          <option value="fadeInLeft">fadeInLeft</option>
					          <option value="fadeInLeftBig">fadeInLeftBig</option>
					          <option value="fadeInRight">fadeInRight</option>
					          <option value="fadeInRightBig">fadeInRightBig</option>
					          <option value="fadeInUp">fadeInUp</option>
					          <option value="fadeInUpBig">fadeInUpBig</option>
					        </optgroup>

					        <optgroup label="Fading Exits">
					          <option value="fadeOut">fadeOut</option>
					          <option value="fadeOutDown">fadeOutDown</option>
					          <option value="fadeOutDownBig">fadeOutDownBig</option>
					          <option value="fadeOutLeft">fadeOutLeft</option>
					          <option value="fadeOutLeftBig">fadeOutLeftBig</option>
					          <option value="fadeOutRight">fadeOutRight</option>
					          <option value="fadeOutRightBig">fadeOutRightBig</option>
					          <option value="fadeOutUp">fadeOutUp</option>
					          <option value="fadeOutUpBig">fadeOutUpBig</option>
					        </optgroup>

					        <optgroup label="Flippers">
					          <option value="flip">flip</option>
					          <option value="flipInX">flipInX</option>
					          <option value="flipInY">flipInY</option>
					          <option value="flipOutX">flipOutX</option>
					          <option value="flipOutY">flipOutY</option>
					        </optgroup>

					        <optgroup label="Lightspeed">
					          <option value="lightSpeedIn">lightSpeedIn</option>
					          <option value="lightSpeedOut">lightSpeedOut</option>
					        </optgroup>

					        <optgroup label="Rotating Entrances">
					          <option value="rotateIn">rotateIn</option>
					          <option value="rotateInDownLeft">rotateInDownLeft</option>
					          <option value="rotateInDownRight">rotateInDownRight</option>
					          <option value="rotateInUpLeft">rotateInUpLeft</option>
					          <option value="rotateInUpRight">rotateInUpRight</option>
					        </optgroup>

					        <optgroup label="Rotating Exits">
					          <option value="rotateOut">rotateOut</option>
					          <option value="rotateOutDownLeft">rotateOutDownLeft</option>
					          <option value="rotateOutDownRight">rotateOutDownRight</option>
					          <option value="rotateOutUpLeft">rotateOutUpLeft</option>
					          <option value="rotateOutUpRight">rotateOutUpRight</option>
					        </optgroup>

					        <optgroup label="Sliding Entrances">
					          <option value="slideInUp">slideInUp</option>
					          <option value="slideInDown">slideInDown</option>
					          <option value="slideInLeft">slideInLeft</option>
					          <option value="slideInRight">slideInRight</option>

					        </optgroup>
					        <optgroup label="Sliding Exits">
					          <option value="slideOutUp">slideOutUp</option>
					          <option value="slideOutDown">slideOutDown</option>
					          <option value="slideOutLeft">slideOutLeft</option>
					          <option value="slideOutRight">slideOutRight</option>
					          
					        </optgroup>
					        
					        <optgroup label="Zoom Entrances">
					          <option value="zoomIn">zoomIn</option>
					          <option value="zoomInDown">zoomInDown</option>
					          <option value="zoomInLeft">zoomInLeft</option>
					          <option value="zoomInRight">zoomInRight</option>
					          <option value="zoomInUp">zoomInUp</option>
					        </optgroup>
					        
					        <optgroup label="Zoom Exits">
					          <option value="zoomOut">zoomOut</option>
					          <option value="zoomOutDown">zoomOutDown</option>
					          <option value="zoomOutLeft">zoomOutLeft</option>
					          <option value="zoomOutRight">zoomOutRight</option>
					          <option value="zoomOutUp">zoomOutUp</option>
					        </optgroup>

					        <optgroup label="Specials">
					          <option value="hinge">hinge</option>
					          <option value="rollIn">rollIn</option>
					          <option value="rollOut">rollOut</option>
					        </optgroup>
					      </select>
					</div>
				</div>
				<div class="form-group">
					<?php echo form_label('Status'); ?>
					<div class="controls">
					   <?php echo form_dropdown('status',$options); ?>
					</div>
				</div>
				<div style="margin-top: 20px;">
					<?php echo form_submit(array('id'=>'submit','value'=>'Save Changes','class'=>'btn btn-success'));  ?>
					<a href="javascript:window.history.go(-1);" class="go-back-a"><span class="go-back">Go Back</span></a>
				</div>			    
			</fieldset>
			<?php echo form_close(); ?> 		
		</div>
	</div>
</div>