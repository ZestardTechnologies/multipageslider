
<link rel="stylesheet" media="all" href="http://zestardshop.com/shopifyapp/multipageslider/zestard/assets/css/payment-page-style.css">
<div class="next-layout--fixed-width">
    <div class="next-grid">
        <div class="next-grid__cell">
            <div class="next-card">
                <div class="next-card__section">
                    <div id="confirm-app-charge" class="confirm-app-charge" refresh="confirm-app-charge" define="{ poller: new Shopify.BillingAction({ autoStartPoller: false }) }">
                        <div class="app-install-icons">
                            <img class="inline-block" alt="" id="app-logo" src="http://zestardshop.com/shopifyapp/multipageslider/zestard/assets/images/icon-60.png" width="60" height="60">
                        </div>
                        <h2 class="next-heading next-heading--2">Activate App from Zestard</h2>
                        <p class="next-heading next-heading--subdued">Multi Page Responsive Slider</p>

                        <div class="actions hide" data-bind-show="poller.polling || chargesForm.submitting">
							Waiting for confirmation ...
                            <img class="inline-block" src="http://zestardshop.com//shopifyapp/multipageslider/zestard/assets/images/loading.gif" alt="Loading small">
                        </div>
                        <div class="actions" data-bind-show="!poller.polling &amp;&amp; !chargesForm.submitting">
                            <div class="ui-stack ui-stack--wrap ui-stack--distribution-center">
                                <?php echo form_open('Category/setup', array('name' => 'registration', 'class' => 'inline-block', 'method' => 'post')); ?>
                                <input type="hidden" name="id" id="id" value="<?php echo $data[0]->charge_id ?>">
                                <input type="hidden" name="accepted" id="accepted" value="true">
                                <input type="submit" name="commit" value="Activate App" class="btn btn-primary js-btn-loadable has-loading">
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div> <!-- confirm-app-charge -->
                </div>
            </div>
            <div class="footer info-message">
                <p>By proceeding, you are agreeing to the <a href="https://www.shopify.com/partners/app-store-terms" data-bind-event-click="Page.openPopup(this.href, &quot;popup&quot;, { width: 800, height: 640 })" target="_blank" rel="noopener noreferrer">Terms of service</a>.
                </p>
                <p>
					*Subject to government tax and other prevailing charges.
                </p>
            </div>
        </div>
    </div>
</div>
