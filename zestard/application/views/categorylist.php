<div id="wrap">		
	<div id="category_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">					
		<div class="col-md-9 col-xs-12" style="padding-top: 10px">
			<div class="btn-set" style="width: 100%;margin-bottom: 10px;display: inline-flex;">
				<h1 style="font-size: 24px;width: 100%;">Manage Sliders</h1>
				<a type="button" href="<?php echo base_url('category/add/'); ?>" value="Add Category" class="btn btn-primary" style="float: right;width: 15%;display: block;vertical-align: middle;padding: 16px;">Add Slider</a>
			</div>						
			<?php if($records == NULL) : ?>
				<div class="alert alert-info" style="background: #f5f5f5; color: #494949; border: 1px solid #ccc">
			        There are no any category.
			    </div>
			<?php else : ?>			
				<table id="category" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%" role="grid" aria-describedby="category_info" style="width: 100%;">
			        <thead>
						  <tr role="row">
							 <th class="sorting_desc" tabindex="0" aria-controls="category" rowspan="1" colspan="1" aria-label="ID: activate to sort column ascending" aria-sort="descending" style="width: 10px;">ID</th>
							 <th class="sorting" tabindex="0" aria-controls="category" rowspan="1" colspan="1" aria-label="Title: activate to sort column ascending" style="width: 397px;">Title</th>
							 <th class="sorting" tabindex="0" aria-controls="category" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending" style="width: 40px;">Status</th>
							 <th class="sorting action-class" style="width: 30px;text-align: cente;">Action</th>
						  </tr>
					</thead>			        
			        <tbody>
			        	<?php $count = 1; ?>
			        	<?php 
			        	foreach($records as $categoryData){			        		
		        		$class = 'odd'; 
		        		if($count%2 == 0){
		        			$class = 'even';
		        		}
			        	?>
						<tr role="row" class="<?php echo $class; ?>">
			                <td class="sorting_1" >
			                    <?php echo $count++; ?>
			                </td>
			                <td>
			                    <?php echo $categoryData->title; ?>
			                </td>
			                <td>
			                    <?php echo ($categoryData->status == 0 ? 'Disable' : 'Enable'); ?>
			                </td>		
			                <td>
								<a href="<?php echo base_url('category/edit/'.$categoryData->crypt_id); ?>">Edit</a> || <a onclick="return confirm('Are you sure you want to delete this category?')" href="<?php echo base_url('category/delete/'.$categoryData->crypt_id); ?>">Delete</a>
			        		</td>
			        	</tr>	            
			        	<?php } ?>			        		
			        </tbody>
			    </table>
			<?php endif; ?>	
		</div>		
	</div>
</div>
<div class="modal fade" id="new_note">
    <div class="modal-dialog">          
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><b>Note</b></h4>
			</div>
			<div class="modal-body">
				<p>Dear Customer, As this is a paid app and hundreds of customers are using it, So if you face any issue(s) on your store before uninstalling, Please contact support team (<a href="mailto:support@zestard.com">support@zestard.com</a>) or live chat at bottom right to resolve it ASAP.</p>
			</div>        
			<div class="modal-footer">			
				<div class="datepicker_validate" id="modal_div">
					<div>
						<strong>Show me this again</strong>
							<span class="onoff"><input name="modal_status" type="checkbox" checked id="dont_show_again"/>							
							<label for="dont_show_again"></label></span>
					</div>      
				</div>      
			</div>      
		</div>
	</div>
</div>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function(){
		$('#category').DataTable();
		$("#dont_show_again").change(function(){
			var checked   = $(this).prop("checked");
			var shop_name = "<?php echo $this->session->userdata['shop'] ?>";			
			if(!checked)
			{
				$.ajax({
					url:'update-modal-status',
					data:{shop_name:shop_name},
					async:false,					
					success:function(result)
					{
						
					}
				});				
				$('#new_note').modal('toggle');
			}
		});		
	});
	var new_install = "<?php echo $new_install ?>";
	if(new_install)
	{				
		if(new_install == "Y")
		{
			$('#new_note').modal('show');
		}
	}		
</script>