<div id="wrap">		
	<div class="col-md-9 col-xs-12">
		<h1 style="font-size: 24px;">Gallery Slide Form</h1>
		<?php if (isset($success)) 
		{ 
			echo "<div class='alert alert-success'>
					$success_msg
				</div>"; 
		}		
		if (isset($error)) 
		{ 
			echo "<div class='alert alert-danger'>
					Error: $error
				</div>"; 
		} 
		?>
		<div class="custom-form-design">			
			<?php echo form_open_multipart('Item/item_update'); ?>
	    	<?php		    
	    		$getCategories = $this->Slider_Image_Items_Model->getCategories($_SESSION['shop']);
				$options = array(
			      '0'  => 'Disable',
			      '1'  => 'Enable'      
			    );
			    $target_window = array(
			      '1'  => 'Same Window',
			      '2'  => 'New Window'      
			    );			    
			    $selected = ($records[0]->status == 0 ? 0 : 1);			    			   
			?>
		    <fieldset>
		    	<?php $getStoreCollection = $this->Slider_Image_Items_Model->getStoreIdByStoreName($_SESSION['shop']); ?>		    	
		      		<?php echo form_hidden('store_id',$getStoreCollection[0]->id); ?>	
		    	<?php echo form_hidden('id',$id);  ?>
				<div class="form-group">
					<?php echo form_label('Title'); ?>
					<div class="controls">
						<?php echo form_input(array('id'=>'title','name'=>'title','class'=>'form-control','value'=>$records[0]->title));  ?>
					</div>
				</div>
				<div class="form-group">
					<?php echo form_label('Select Slider'); ?>
					<div class="controls">							
						<select name="category_id" style="width: auto;padding: 5px;background-color: #FFF;">								
							<option value="0">Uncategorized</option>
							<?php foreach($getCategories as $category) : ?>
								<option value="<?php echo $category->id; ?>" <?php echo ($category->id == $records[0]->category_id ? 'selected' : ''); ?>><?php echo ucfirst($category->title); ?></option>
							<?php endforeach; ?>
						</select>							
					</div>
				</div>
				<div class="form-group">
					<?php echo form_label('Shopnow Title'); ?>						
					<div class="controls">
						<?php echo form_input(array('id'=>'shopnow_title','name'=>'shopnow_title','class'=>'form-control','placeholder'=>'Enter Shopnow Title','value'=>$records[0]->shopnow_title));?>
					</div>
				</div>
				<div class="form-group">
					<?php echo form_label('Shopnow URL'); ?>						
					<div class="controls">
						<?php echo form_input(array('id'=>'shopnow_url','name'=>'shopnow_url','class'=>'form-control','placeholder'=>'Enter Shopnow URL','value'=>$records[0]->shopnow_url));?>							
					</div>
				</div>
				<div class="form-group">
					<?php echo form_label('Show Shopnow'); ?>					
					<div class="controls">						
						<?php echo form_dropdown('item_show_shopnow',$options,($records != NULL) ? ($records[0]->item_show_shopnow == 0 ? 0 : 1) : ''); ?>
						<em>By default option is Disable</em>
					</div>						
				</div>
				<div class="form-group">
					<?php echo form_label('Show Title'); ?>					
					<div class="controls">						
						<?php echo form_dropdown('item_show_title',$options,($records != NULL) ? ($records[0]->item_show_title == 0 ? 0 : 1) : ''); ?>
						<em>By default option is Disable</em>
					</div>						
				</div>
				<div class="form-group">
					<?php echo form_label('Target Window'); ?>					
					<div class="controls">
						<?php echo form_dropdown('item_target_window',$target_window,($records != NULL) ?($records[0]->item_target_window == 1 ? 1 : 2) : '', 'style="width: 140px;"'); ?>						
						<em>By default option is same window</em>
					</div>
				</div>
				<div class="form-group">
					<?php echo form_label('Content On Image'); ?>						
					<div class="controls">
						<?php echo form_textarea(array('id'=>'image_content','name'=>'image_content','rows'=>10,'cols'=>60,'value'=>$records[0]->image_content));?>
					</div>
				</div>				
				<div class="form-group">
					<?php echo form_label('Choose Image'); ?>						
					<div class="controls" style="width: 100%;display: inline-block;">							
						<img id="userfile_preview" class="media-object img-thumbnail pull-left" height='60px' width='60px' style="margin-right: 10px;" 
						src="<?php if(!empty($records[0]->image)){  echo base_url(); ?>assets/uploads/<?php echo $records[0]->store_id.'/'.$records[0]->image; } else {  echo base_url(); ?>assets/images/No_Image.png<?php } ?>" alt="" />
						<input type="file" class="form-control" name="image" id="image" onchange="readURL(this,'userfile_preview');" style="width: auto;" />													
					</div>				
				</div>
                                <div class="form-group">
					<?php echo form_label('Image URL'); ?>						
					<div class="controls">
						<?php echo form_input(array('id'=>'image_url','name'=>'image_url','class'=>'form-control','placeholder'=>'Enter Shopnow URL','value'=>$records[0]->image_link_url));?>							
                                                <em>Please note that if you use above "Banner URL" field then "Shopnow URL" will not work and "Banner URL" will be assigned to entire banner area</em>
					</div>
				</div>
				<div class="form-group">
					<?php echo form_label('Banner Url Status'); ?>					
					<div class="controls">						
						<?php echo form_dropdown('image_link_status',$options,($records != NULL) ? ($records[0]->image_link_status == 0 ? 0 : 1) : ''); ?>
						<em>By default option is Disable</em>
					</div>						
				</div>
                                <div class="form-group">
					<?php echo form_label('Target Window for Banner Url'); ?>					
					<div class="controls">
						<?php echo form_dropdown('banner_target_window',$target_window,($records != NULL) ?($records[0]->banner_target_window == 1 ? 1 : 2) : '', 'style="width: 140px;"'); ?>						
						<em>By default option is same window</em>
					</div>
				</div>                                                      
				<div class="form-group">
					<?php echo form_label('Status'); ?>
					<div class="controls">
						<?php echo form_dropdown('status',$options,$selected); ?> 
					</div>
				</div>
                                <!--start -->
                                <div class="form-group">
                                    <?php echo form_label('Slide Display Order'); ?>						
                                    <div class="controls">
                                        <input type="number" id="display_order" name="display_order" class="form-control" style="max-width: 125px;" value="<?php echo ($records[0]->display_order == NULL ? '' : $records[0]->display_order); ?>">
                                        <em>(It will be used to display slide in given order. Please enter number between 1 to 100)</em>
                                    </div>
				</div>
                                <!--end -->
				<div style="margin-top: 20px;">
					<?php echo form_submit(array('id'=>'submit','value'=>'Save Changes','class'=>'btn btn-success'));  ?>
					<a href="javascript:window.history.go(-1);" class="go-back-a"><span class="go-back">Go Back</span></a>
				</div>			    
			</fieldset>
			<?php echo form_close(); ?> 		
		</div>
	</div>
</div>
<script>
function readURL(input,id) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
      $('#'+id).attr('src', e.target.result).width(50).height(50);
    };
    reader.readAsDataURL(input.files[0]);
  }
}
</script>