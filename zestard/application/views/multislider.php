<?php
if ($category_data[0]->catgory_inline_css != NULL) {
  echo "<style type='text/css'>"
  .$category_data[0]->catgory_inline_css
  ."</style>";
}
?>
<?php //$getSettings = $this->Multislider_Model->getSettings($store_id); ?>
<section class="slider <?php echo $slider_style; ?> <?php echo $category_data[0]->catgory_class_name; ?>">
	<div class="flexslider zestard_multipage_flexslider">		
		<ul class="slides">
			<?php foreach($getItemData as $item): ?>
			<li>
				<img alt="<?php echo $item->title; ?>" src="<?php echo base_url('assets/uploads/'.$item->store_id.'/'.$item->image) ?>">
				<div class="flex-caption">
					<?php if($item->item_show_title): ?>					
						<h2><?php echo $item->title; ?></h2>
					<?php endif; ?>	
					<div class="textContainer deskcontent" datalimit="200">
						<?php echo $item->image_content; ?>
					</div>						
				</div>				
				<?php if($item->item_show_shopnow): ?>
					<div class="shopnow">
						<a href="<?php echo $item->shopnow_url; ?>" target="<?php echo ($item->item_target_window == 1 ? '_parent' : '_blank') ?>"><?php echo $item->shopnow_title; ?></a>
					</div>
				<?php endif; ?>				
			</li>
			<?php endforeach; ?>				
		</ul>
		<ol class="flex-control-paging cslide">
			<?php if($slider_style == 'slider3'): ?>
				<?php foreach($getItemData as $item): ?>
					<li>
						<a>
							<img alt="<?php echo $item->title; ?>" src="<?php echo base_url('/assets/uploads/'.$store_id.'/'.$item->image);  ?>" class="scroll-thumb-img"/>
						</a>
					</li>
				<?php endforeach; ?>
			<?php else : ?>
				<?php foreach($getItemData as $item): ?>
					<li>
						<a>
							<?php echo $item->title; ?>
						</a>
					</li>
				<?php endforeach; ?>
			<?php endif; ?>
		</ol>		
	</div>
	<input type="hidden" name="cat_slide_time" id="cat_slide_time" class="cat_slide_time" value="<?php echo $category_data[0]->catgory_slide_time; ?>">
</section>
<script type="text/javascript">


jQuery(window).load(function(){
    var slideTime = jQuery("#cat_slide_time").val();
    window.gObj = new Gallery();
    window.gObj.initslider(slideTime);
	console.log(Shopify.shop);
    jQuery('.cslide li').click(function()
     {
        var val = parseInt(jQuery(this).index()+1);
        jQuery(this).parent().parent().find('.flex-control-nav li:nth-child('+val+') a').click();

    });
});	
</script>