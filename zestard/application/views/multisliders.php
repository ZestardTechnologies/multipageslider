<?php
if ($category_data[0]->catgory_inline_css != NULL) {
  echo "<style type='text/css'>"
  .$category_data[0]->catgory_inline_css
  ."</style>";
}
?>
<section class="slider <?php echo $slider_style; ?> <?php echo $category_data[0]->catgory_class_name; ?>">
	<div class="flexslider zestard_multipage_flexslider">		
		<ul class="slides">
			<?php foreach($getItemData as $item): ?>
			<li>				
                            <?php if($item->image_link_status == 1): ?>
                            <a href="<?php echo $item->image_link_url; ?>" target="<?php echo ($item->banner_target_window == 1 ? '_parent' : '_blank') ?>">
                                <?php 
                                   if (strpos($item->image, 'cdn.shopify.com') !== false) {
                                       $slider_url = $item->image;
                                   }else{
                                       $slider_url = base_url('assets/uploads/'.$item->store_id.'/'.$item->image);
                                   }
                                ?>
                                <img alt="<?php echo $item->title; ?>" src="<?php echo $slider_url; ?>">				
				<?php if($item->item_show_title || $item->image_content): ?>
					<div class="flex-caption">
						<?php if($item->item_show_title): ?>
							<h2><?php echo $item->title; ?></h2>
						<?php endif; ?>	
						<?php if($item->image_content): ?>
							<div class="textContainer deskcontent" datalimit="200">
								<?php echo $item->image_content; ?>
							</div>
						<?php endif; ?>
					</div>
				<?php endif; ?>
                            </a>
                            <?php else : ?>
				<img alt="<?php echo $item->title; ?>" src="<?php echo base_url('assets/uploads/'.$item->store_id.'/'.$item->image) ?>">				
				<?php if($item->item_show_title || $item->image_content): ?>
					<div class="flex-caption">
						<?php if($item->item_show_title): ?>
							<h2><?php echo $item->title; ?></h2>
						<?php endif; ?>	
						<?php if($item->image_content): ?>
							<div class="textContainer deskcontent" datalimit="200">
								<?php echo $item->image_content; ?>
							</div>
						<?php endif; ?>
					</div>
				<?php endif; ?>
				<?php if($item->item_show_shopnow): ?>
					<div class="shopnow">
						<a href="<?php echo $item->shopnow_url; ?>" target="<?php echo ($item->item_target_window == 1 ? '_parent' : '_blank') ?>"><?php echo $item->shopnow_title; ?></a>
					</div>
				<?php endif; ?>
                            <?php endif; ?>
			</li>
                        
			<?php endforeach; ?>				
		</ul>
		<ol class="flex-control-paging cslide">
			<?php if($slider_style == 'slider3'): ?>
				<?php foreach($getItemData as $item): ?>
					<li>
						<a>
							<img alt="<?php echo $item->title; ?>" src="<?php echo base_url('/assets/uploads/'.$store_id.'/'.$item->image);  ?>" class="scroll-thumb-img"/>
						</a>
					</li>
				<?php endforeach; ?>
			<?php elseif($slider_style == 'slider4'): ?>
				<?php foreach($getItemData as $item): ?>
					<li>
						<a>
							<img alt="<?php echo $item->title; ?>" src="<?php echo base_url('/assets/uploads/'.$store_id.'/'.$item->image);  ?>" class="scroll-thumb-img"/>
						</a>
					</li>
				<?php endforeach; ?>
			<?php else : ?>
				<?php foreach($getItemData as $item): ?>
					<li>
						<a>
							<?php echo $item->title; ?>
						</a>
					</li>
				<?php endforeach; ?>
			<?php endif; ?>
		</ol>		
	</div>
	<input type="hidden" name="catgory_id" class="catgory_id" value="<?php echo $category_data[0]->id; ?>">
	<input type="hidden" name="item_effects" class="item_effects" value="<?php echo $category_data[0]->item_effects; ?>">
	<input type="hidden" name="cat_slide_time" id="cat_slide_time" class="cat_slide_time" value="<?php echo $category_data[0]->catgory_slide_time; ?>">
</section>