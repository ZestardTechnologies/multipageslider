<?php
ob_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <!--Load the CSS-->                
        <!--Load the JS-->
        <script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
        <script src="https://cdn.shopify.com/s/assets/external/app.js"></script>
        
        <?php
            $api_key = $_SESSION['api_key']; //$this->session->userdata('api_key');
            $shop = $_SESSION['shop']; //$this->session->userdata('shop');
        ?>
        <script type="text/javascript">			
			ShopifyApp.init({
                apiKey: '<?php if(isset ($api_key)){echo $api_key;} ?>',
				shopOrigin: 'https://<?php if(isset ($shop)){ echo $shop; }?>'
			});	
			ShopifyApp.ready(function() {
				ShopifyApp.Bar.initialize({
		        icon: 'https://www.zestardshop.com/shopifyapp/twitterfeed/zestard/assets/images/icon.png',
		        title: 'Management',
		        buttons: { }
		      });
		    });		    
	</script> 
    <title><?php echo $title; ?></title>
    </head>    
