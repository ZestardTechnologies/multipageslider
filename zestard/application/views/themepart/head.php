<?php 
//session_start();
//ob_start(); ?>
<!DOCTYPE html>
<html>
    <head>            	
        <link rel="stylesheet" href="<?php echo base_url($css . 'font-awesome.min.css'); ?>">
		<link rel="stylesheet" href="<?php echo base_url($css . 'style.css'); ?>">
		<link rel="stylesheet" href="<?php echo base_url($css . 'bootstrap.min.css'); ?>">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url($css . 'datatables.min.css'); ?>">
		<script src="https://code.jquery.com/jquery-2.1.0.min.js"></script>
		<script src="<?php echo base_url($js . 'jquery.min.js'); ?>"></script>
		<script src="https://cdn.shopify.com/s/assets/external/app.js"></script>		
		<script type="text/javascript" src="<?php echo base_url($js . 'datatables.min.js'); ?>"></script>
		<?php			
			$api_key 	= $_SESSION['api_key']; //$this->session->userdata('api_key');
			$shop 		= $_SESSION['shop']; //$this->session->userdata('shop');

		//$api_key = $this->session->userdata('api_key');
	    //$shop = $this->session->userdata('shop');
		
			
		?>
		<script type="text/javascript">			
			ShopifyApp.init({
				apiKey: '<?php echo $api_key; ?>',
				shopOrigin: 'https://<?php echo $shop; ?>'
			});	

			ShopifyApp.ready(function() {
		      ShopifyApp.Bar.initialize({
		        icon: 'https://www.zestardshop.com/shopifyapp/multipageslider/zestard/assets/images/gallery.png',
		        title: 'Management',
		        buttons: { }
		      });
		    });		    
		</script>
                
                <!--Start of Tawk.to Script-->
                <script type="text/javascript">
                var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
                (function(){
                var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
                s1.async=true;
                s1.src='https://embed.tawk.to/5a2e20e35d3202175d9b7782/default';
                s1.charset='UTF-8';
                s1.setAttribute('crossorigin','*');
                s0.parentNode.insertBefore(s1,s0);
                })();
                </script>
                <!--End of Tawk.to Script-->
		<title><?php echo $title; ?></title>
    </head>    
