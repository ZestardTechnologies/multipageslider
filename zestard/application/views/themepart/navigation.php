<body>
	<div class="navbar navbar-default navbar-static-top" style="margin:0">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Navigation</span>
					<span class="fa fa-bar"></span>
					<span class="fa fa-bar"></span>
					<span class="fa fa-bar"></span>
				</button>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav" id="navbars">            					
					<li class="<?php echo ($page == 'category' ? 'active' : '') ?>">
						<a href="<?php echo base_url('category/'); ?>">
							<i class="fa fa-cog"></i>
							&nbsp;Manage Slider
						</a>
					</li>
					<li class="<?php echo ($page == 'item' ? 'active' : '') ?>">
						<a href="<?php echo base_url('item/'); ?>">
							<i class="fa fa-cog"></i>&nbsp;Manage Slides (Gallery)
						</a>
					</li>
					<li class="<?php echo ($page == 'help' ? 'active' : '') ?>">
						<a href="<?php echo base_url('help/'); ?>">
							<i class="fa fa-info-circle"></i>&nbsp;Help
						</a>
					</li>
					
<!--					<li class="<?php //echo ($page == 'settings' ? 'active' : '') ?>">
						<a href="<?php //echo base_url('settings/'); ?>">
							<i class="fa fa-cog"></i>&nbsp;Settings
						</a>
					</li>-->
					
				</ul>
			</div>
		</div>
	</div>
<style>
        .review-div {
			
            text-align: center !important;
            width: 100%;
            background-color: #fafad0;
            padding: 10px;
            box-shadow: 1px 1px 2px #777;
            display: inline-block;
            position: sticky;
            top: 0px;
            left: 0px;
            z-index: 100;
         }
    </style>
	<div class="review-div">
    	Are you loving this app? Please spend 2mins to help us <a href="https://apps.shopify.com/multi-page-responsive-slider?reveal_new_review=true" target="_blank">write a review</a>. 
	</div>