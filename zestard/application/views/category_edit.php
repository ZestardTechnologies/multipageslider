<div id="wrap">		
	<div class="col-md-9 col-xs-12">
		<h1 style="font-size: 24px;">Slider Form</h1>
		<div class="custom-form-design">
			<?php echo form_open('Category/category_update'); ?>
	    	<?php		    
				$options = array(
			      '0'  => 'Disable',
			      '1'  => 'Enable'      
			    );
			    $selected = ($records[0]->status == 0 ? 0 : 1);		    	
			    $slider_style_option = array(
			      'slider1'  => 'Slider1',
			      'slider2'  => 'Slider2',      
			      'slider3'	 => 'Slider3',
			      'slider4'	 => 'Slider4'
			    );			    
			    
			    
			    $slider_style = '';
			    if($records[0]->slider_style == 'slider1')
			    	$slider_style = 'slider1';	
			    elseif($records[0]->slider_style == 'slider2')
			    	$slider_style = 'slider2';
			    elseif($records[0]->slider_style == 'slider3')
			    	$slider_style = 'slider3';
			    elseif($records[0]->slider_style == 'slider4')
			    	$slider_style = 'slider4';
			    else
			    	$slider_style = '';
			?>
		    <fieldset>
		    	<?php echo form_hidden('id',$id);  ?>
				<div class="form-group">
					<?php echo form_label('Title'); ?>
					<div class="controls">
						<?php echo form_input(array('id'=>'title','name'=>'title','class'=>'form-control','value'=>$records[0]->title));  ?>
					</div>
				</div>
				<div class="form-group">
					<?php echo form_label('Slider Style: <strong>Different Slider Styles</strong> <span>(Choose one of them)</span>'); ?>
					<div class="controls">
						<?php echo form_dropdown('slider_style',$slider_style_option,$slider_style); ?>
						<div class="slider-demo">							
							<div class="slider" image-src="http://zestardshop.com//shopifyapp/multipageslider/zestard/assets/images/slider1.png">Slider1 Style Demo</div>
							<div class="slider" image-src="http://zestardshop.com//shopifyapp/multipageslider/zestard/assets/images/slider2.png">Slider2 Style Demo</div>
							<div class="slider" image-src="http://zestardshop.com//shopifyapp/multipageslider/zestard/assets/images/slider3.png">Slider3 Style Demo</div>
							<div class="slider" image-src="http://zestardshop.com//shopifyapp/multipageslider/zestard/assets/images/slider4.png">Slider4 Style Demo</div>
						</div>						
					</div>
				</div>				
				<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">              
							<div class="modal-body">
								<button type="button" class="close" data-dismiss="modal">
									<span aria-hidden="true">&times;</span>
									<span class="sr-only">Close</span>
								</button>
								<img src="" class="imagepreview" style="width: 100%;" >
							</div>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<?php echo form_label('Slide Time <span>(Set the speed of the slideshow cycling, <strong>in milliseconds</strong>.)</span>'); ?>
					<div class="controls">
						<?php echo form_input(array('id'=>'catgory_slide_time','name'=>'catgory_slide_time','class'=>'form-control','value'=>$records[0]->catgory_slide_time));?>
					</div>
				</div>
				<div class="form-group">
					<?php echo form_label('Class Name'); ?>
					<div class="controls">
						<?php echo form_input(array('id'=>'catgory_class_name','name'=>'catgory_class_name','class'=>'form-control','value'=>$records[0]->catgory_class_name));?>
					</div>
				</div>
				<div class="form-group">
					<?php echo form_label('Internal CSS <span>(In this box you can write your own CSS, For Example: <strong>.body {color: #ccccc;}</strong>)<span>'); ?>
					<div class="controls">						
						<textarea id="catgory_inline_css" rows="6" cols="10" class="form-control" name="catgory_inline_css"><?php echo $records[0]->catgory_inline_css; ?></textarea>
					</div>					
				</div>
				<div class="form-group">
					<?php echo form_label('Status'); ?>
					<div class="controls">
						<?php echo form_dropdown('status',$options,$selected); ?>
					</div>
				</div>
				<div class="form-group">
					<?php echo form_label('Content Effect'); ?>						
					<div class="controls">
						<select name="item_effects" style="width: 200px;">
					        <optgroup label="Attention Seekers">
					          <option value="bounce" <?php echo ($records[0]->item_effects == 'bounce' ? 'selected' : ''); ?>>bounce</option>
					          <option value="flash" <?php echo ($records[0]->item_effects == 'flash' ? 'selected' : ''); ?>>flash</option>
					          <option value="pulse" <?php echo ($records[0]->item_effects == 'pulse' ? 'selected' : ''); ?>>pulse</option>
					          <option value="rubberBand" <?php echo ($records[0]->item_effects == 'rubberBand' ? 'selected' : ''); ?>>rubberBand</option>
					          <option value="shake" <?php echo ($records[0]->item_effects == 'shake' ? 'selected' : ''); ?>>shake</option>
					          <option value="swing" <?php echo ($records[0]->item_effects == 'swing' ? 'selected' : ''); ?>>swing</option>
					          <option value="tada" <?php echo ($records[0]->item_effects == 'tada' ? 'selected' : ''); ?>>tada</option>
					          <option value="wobble" <?php echo ($records[0]->item_effects == 'wobble' ? 'selected' : ''); ?>>wobble</option>
					          <option value="jello" <?php echo ($records[0]->item_effects == 'jello' ? 'selected' : ''); ?>>jello</option>
					        </optgroup>

					        <optgroup label="Bouncing Entrances">
					          <option value="bounceIn" <?php echo ($records[0]->item_effects == 'bounceIn' ? 'selected' : ''); ?>>bounceIn</option>
					          <option value="bounceInDown" <?php echo ($records[0]->item_effects == 'bounceInDown' ? 'selected' : ''); ?>>bounceInDown</option>
					          <option value="bounceInLeft" <?php echo ($records[0]->item_effects == 'bounceInLeft' ? 'selected' : ''); ?>>bounceInLeft</option>
					          <option value="bounceInRight" <?php echo ($records[0]->item_effects == 'bounceInRight' ? 'selected' : ''); ?>>bounceInRight</option>
					          <option value="bounceInUp" <?php echo ($records[0]->item_effects == 'bounceInUp' ? 'selected' : ''); ?>>bounceInUp</option>
					        </optgroup>

					        <optgroup label="Bouncing Exits">
					          <option value="bounceOut" <?php echo ($records[0]->item_effects == 'bounceOut' ? 'selected' : ''); ?>>bounceOut</option>
					          <option value="bounceOutDown" <?php echo ($records[0]->item_effects == 'bounceOutDown' ? 'selected' : ''); ?>>bounceOutDown</option>
					          <option value="bounceOutLeft" <?php echo ($records[0]->item_effects == 'bounceOutLeft' ? 'selected' : ''); ?>>bounceOutLeft</option>
					          <option value="bounceOutRight" <?php echo ($records[0]->item_effects == 'bounceOutRight' ? 'selected' : ''); ?>>bounceOutRight</option>
					          <option value="bounceOutUp" <?php echo ($records[0]->item_effects == 'bounceOutUp' ? 'selected' : ''); ?>>bounceOutUp</option>
					        </optgroup>

					        <optgroup label="Fading Entrances">
					          <option value="fadeIn" <?php echo ($records[0]->item_effects == 'fadeIn' ? 'selected' : ''); ?>>fadeIn</option>
					          <option value="fadeInDown" <?php echo ($records[0]->item_effects == 'fadeOutUpBig' ? 'selected' : ''); ?>>fadeInDown</option>
					          <option value="fadeInDownBig" <?php echo ($records[0]->item_effects == 'fadeInDownBig' ? 'selected' : ''); ?>>fadeInDownBig</option>
					          <option value="fadeInLeft" <?php echo ($records[0]->item_effects == 'fadeInLeft' ? 'selected' : ''); ?>>fadeInLeft</option>
					          <option value="fadeInLeftBig" <?php echo ($records[0]->item_effects == 'fadeInLeftBig' ? 'selected' : ''); ?>>fadeInLeftBig</option>
					          <option value="fadeInRight" <?php echo ($records[0]->item_effects == 'fadeInRight' ? 'selected' : ''); ?>>fadeInRight</option>
					          <option value="fadeInRightBig" <?php echo ($records[0]->item_effects == 'fadeInRightBig' ? 'selected' : ''); ?>>fadeInRightBig</option>
					          <option value="fadeInUp" <?php echo ($records[0]->item_effects == 'fadeInUp' ? 'selected' : ''); ?>>fadeInUp</option>
					          <option value="fadeInUpBig" <?php echo ($records[0]->item_effects == 'fadeInUpBig' ? 'selected' : ''); ?>>fadeInUpBig</option>
					        </optgroup>

					        <optgroup label="Fading Exits">
					          <option value="fadeOut" <?php echo ($records[0]->item_effects == 'fadeOut' ? 'selected' : ''); ?>>fadeOut</option>
					          <option value="fadeOutDown" <?php echo ($records[0]->item_effects == 'fadeOutDown' ? 'selected' : ''); ?>>fadeOutDown</option>
					          <option value="fadeOutDownBig" <?php echo ($records[0]->item_effects == 'fadeOutDownBig' ? 'selected' : ''); ?>>fadeOutDownBig</option>
					          <option value="fadeOutLeft" <?php echo ($records[0]->item_effects == 'fadeOutLeft' ? 'selected' : ''); ?>>fadeOutLeft</option>
					          <option value="fadeOutLeftBig" <?php echo ($records[0]->item_effects == 'fadeOutLeftBig' ? 'selected' : ''); ?>>fadeOutLeftBig</option>
					          <option value="fadeOutRight" <?php echo ($records[0]->item_effects == 'fadeOutRight' ? 'selected' : ''); ?>>fadeOutRight</option>
					          <option value="fadeOutRightBig" <?php echo ($records[0]->item_effects == 'fadeOutRightBig' ? 'selected' : ''); ?>>fadeOutRightBig</option>
					          <option value="fadeOutUp" <?php echo ($records[0]->item_effects == 'fadeOutUp' ? 'selected' : ''); ?>>fadeOutUp</option>
					          <option value="fadeOutUpBig" <?php echo ($records[0]->item_effects == 'fadeOutUpBig' ? 'selected' : ''); ?>>fadeOutUpBig</option>
					        </optgroup>

					        <optgroup label="Flippers">
					          <option value="flip" <?php echo ($records[0]->item_effects == 'flip' ? 'selected' : ''); ?>>flip</option>
					          <option value="flipInX" <?php echo ($records[0]->item_effects == 'flipInX' ? 'selected' : ''); ?>>flipInX</option>
					          <option value="flipInY" <?php echo ($records[0]->item_effects == 'flipInY' ? 'selected' : ''); ?>>flipInY</option>
					          <option value="flipOutX" <?php echo ($records[0]->item_effects == 'flipOutX' ? 'selected' : ''); ?>>flipOutX</option>
					          <option value="flipOutY" <?php echo ($records[0]->item_effects == 'flipOutY' ? 'selected' : ''); ?>>flipOutY</option>
					        </optgroup>

					        <optgroup label="Lightspeed">
					          <option value="lightSpeedIn" <?php echo ($records[0]->item_effects == 'lightSpeedIn' ? 'selected' : ''); ?>>lightSpeedIn</option>
					          <option value="lightSpeedOut" <?php echo ($records[0]->item_effects == 'lightSpeedOut' ? 'selected' : ''); ?>>lightSpeedOut</option>
					        </optgroup>

					        <optgroup label="Rotating Entrances">
					          <option value="rotateIn" <?php echo ($records[0]->item_effects == 'rotateIn' ? 'selected' : ''); ?>>rotateIn</option>
					          <option value="rotateInDownLeft" <?php echo ($records[0]->item_effects == 'rotateInDownLeft' ? 'selected' : ''); ?>>rotateInDownLeft</option>
					          <option value="rotateInDownRight" <?php echo ($records[0]->item_effects == 'rotateInDownRight' ? 'selected' : ''); ?>>rotateInDownRight</option>
					          <option value="rotateInUpLeft" <?php echo ($records[0]->item_effects == 'rotateInUpLeft' ? 'selected' : ''); ?>>rotateInUpLeft</option>
					          <option value="rotateInUpRight" <?php echo ($records[0]->item_effects == 'rotateInUpRight' ? 'selected' : ''); ?>>rotateInUpRight</option>
					        </optgroup>

					        <optgroup label="Rotating Exits">
					          <option value="rotateOut" <?php echo ($records[0]->item_effects == 'rotateOut' ? 'selected' : ''); ?>>rotateOut</option>
					          <option value="rotateOutDownLeft" <?php echo ($records[0]->item_effects == 'rotateOutDownLeft' ? 'selected' : ''); ?>>rotateOutDownLeft</option>
					          <option value="rotateOutDownRight" <?php echo ($records[0]->item_effects == 'rotateOutDownRight' ? 'selected' : ''); ?>>rotateOutDownRight</option>
					          <option value="rotateOutUpLeft" <?php echo ($records[0]->item_effects == 'rotateOutUpLeft' ? 'selected' : ''); ?>>rotateOutUpLeft</option>
					          <option value="rotateOutUpRight" <?php echo ($records[0]->item_effects == 'rotateOutUpRight' ? 'selected' : ''); ?>>rotateOutUpRight</option>
					        </optgroup>

					        <optgroup label="Sliding Entrances">
					          <option value="slideInUp" <?php echo ($records[0]->item_effects == 'slideInUp' ? 'selected' : ''); ?>>slideInUp</option>
					          <option value="slideInDown" <?php echo ($records[0]->item_effects == 'slideInDown' ? 'selected' : ''); ?>>slideInDown</option>
					          <option value="slideInLeft" <?php echo ($records[0]->item_effects == 'slideInLeft' ? 'selected' : ''); ?>>slideInLeft</option>
					          <option value="slideInRight" <?php echo ($records[0]->item_effects == 'slideInRight' ? 'selected' : ''); ?>>slideInRight</option>

					        </optgroup>
					        <optgroup label="Sliding Exits">
					          <option value="slideOutUp" <?php echo ($records[0]->item_effects == 'slideOutUp' ? 'selected' : ''); ?>>slideOutUp</option>
					          <option value="slideOutDown" <?php echo ($records[0]->item_effects == 'slideOutDown' ? 'selected' : ''); ?>>slideOutDown</option>
					          <option value="slideOutLeft" <?php echo ($records[0]->item_effects == 'slideOutLeft' ? 'selected' : ''); ?>>slideOutLeft</option>
					          <option value="slideOutRight" <?php echo ($records[0]->item_effects == 'slideOutRight' ? 'selected' : ''); ?>>slideOutRight</option>
					          
					        </optgroup>
					        
					        <optgroup label="Zoom Entrances">
					          <option value="zoomIn" <?php echo ($records[0]->item_effects == 'zoomIn' ? 'selected' : ''); ?>>zoomIn</option>
					          <option value="zoomInDown" <?php echo ($records[0]->item_effects == 'zoomInDown' ? 'selected' : ''); ?>>zoomInDown</option>
					          <option value="zoomInLeft" <?php echo ($records[0]->item_effects == 'zoomInLeft' ? 'selected' : ''); ?>>zoomInLeft</option>
					          <option value="zoomInRight" <?php echo ($records[0]->item_effects == 'zoomInRight' ? 'selected' : ''); ?>>zoomInRight</option>
					          <option value="zoomInUp" <?php echo ($records[0]->item_effects == 'zoomInUp' ? 'selected' : ''); ?>>zoomInUp</option>
					        </optgroup>
					        
					        <optgroup label="Zoom Exits">
					          <option value="zoomOut" <?php echo ($records[0]->item_effects == 'zoomOut' ? 'selected' : ''); ?>>zoomOut</option>
					          <option value="zoomOutDown" <?php echo ($records[0]->item_effects == 'zoomOutDown' ? 'selected' : ''); ?>>zoomOutDown</option>
					          <option value="zoomOutLeft" <?php echo ($records[0]->item_effects == 'zoomOutLeft' ? 'selected' : ''); ?>>zoomOutLeft</option>
					          <option value="zoomOutRight" <?php echo ($records[0]->item_effects == 'zoomOutRight' ? 'selected' : ''); ?>>zoomOutRight</option>
					          <option value="zoomOutUp" <?php echo ($records[0]->item_effects == 'zoomOutUp' ? 'selected' : ''); ?>>zoomOutUp</option>
					        </optgroup>

					        <optgroup label="Specials">
					          <option value="hinge" <?php echo ($records[0]->item_effects == 'hinge' ? 'selected' : ''); ?>>hinge</option>
					          <option value="rollIn" <?php echo ($records[0]->item_effects == 'rollIn' ? 'selected' : ''); ?>>rollIn</option>
					          <option value="rollOut" <?php echo ($records[0]->item_effects == 'rollOut' ? 'selected' : ''); ?>>rollOut</option>
					        </optgroup>
					      </select>
					</div>
				</div>
				<div class="form-group">
					<?php echo form_label('Shortcode: <span>(Paste this shortcode in theme files or any page)</span>
					See Example: <a class="shortcode" image-src="http://zestardshop.com//shopifyapp/multipageslider/zestard/assets/images/page.png" href="javascript:void(0)">Page</a> and <a class="shortcode" href="javascript:void(0)" image-src="http://zestardshop.com//shopifyapp/multipageslider/zestard/assets/images/themefile.png">Theme Files</a>'); ?>
					<div class="showCodeWrapper">
						<btn class="btn btn-default copyMe" onclick="copyToClipboard()" style="display: block;"><i class="fa fa-check"></i> Copied</btn>
						<textarea id="shortcode" rows="4" class="form-control short-code" data-app-type="banner-slider" readonly=""><div class="zestard-multislider" id="<?php echo $records[0]->crypt_id; ?>"></div></textarea>
					</div>
				</div>
				<div style="margin-top: 20px;">
					<?php echo form_submit(array('id'=>'submit','value'=>'Save Changes','class'=>'btn btn-success'));  ?>
					<a href="javascript:window.history.go(-1);" class="go-back-a"><span class="go-back">Go Back</span></a>
				</div>			    
			</fieldset>
			<?php echo form_close(); ?> 		
		</div>
	</div>
</div>
<script type="text/javascript">
	function copyToClipboard() {
        var st = document.getElementById("shortcode").value;
        window.prompt("Copy to clipboard: Ctrl+C, Enter", st);
	}
	
	jQuery(function() {
		jQuery('.shortcode').on('click', function() {
			jQuery('.imagepreview').attr('src', jQuery(this).attr('image-src'));
			jQuery('#imagemodal').modal('show');   
		});		
		
		jQuery('.slider').on('click', function() {
			jQuery('.imagepreview').attr('src', jQuery(this).attr('image-src'));
			jQuery('#imagemodal').modal('show');   
		});		
	});
</script>
