<?php
error_reporting(0);
session_start();

defined('BASEPATH') OR exit('No direct script access allowed');
require __DIR__ . '/../../../vendor/shopifymodel/shopify_api/client.php';
require __DIR__ . '/../../../vendor/shopifymodel/wcurl/wcurl.php';

use shopifymodel\shopify_api;

class Base_Controller extends CI_Controller {

    
    function __construct() {
        
        parent::__construct();
		
        $this->load->helper('form');
        $this->load->database();
        $this->load->model('Charge_Model');
        $this->load->library("session");

		$parts = parse_url($_SERVER['REQUEST_URI']);		
		parse_str($parts['query'], $query);
		
		if(empty($_SESSION) || $_SESSION['shop'] == '')
                {
			$_SESSION['shop'] = $query['shop'];
                        $this->session->set_userdata("shop",$query['shop']);
                }
        
        $query = $this->db->select('*');
        $query = $this->db->from('usersettings');
        $query = $this->db->where(array('store_name' => $this->session->userdata("shop")));
        $query = $this->db->get();
        $userData = $query->result();
        //echo '<pre>';print_r($userData);die;
        if($userData[0]->status == "pending")
        {			
            $this->setup();
        }
    }

    public function checkStatus() {
        
        $this->assets = array(
            'css' => $this->config->item('css'),
            'js' => $this->config->item('js'),
            'title' => 'Manage payment'
        );

        $data['data'] = $this->Charge_Model->getDataByShop($this->session->userdata("shop"));
            
            $select_settings = $this->db->query("SELECT * FROM appsettings WHERE id = 1")->row();
            $query = $this->db->select('*');
            $query = $this->db->from('usersettings');
            $query = $this->db->where(array('store_name' => $this->session->userdata("shop")));
            $query = $this->db->get();
            $userData = $query->result();
            $API_KEY = $select_settings->api_key;
            $SECRET = $select_settings->shared_secret;
            $TOKEN = $userData[0]->access_token;
            $store_name = $userData[0]->store_name;
            $shopify = shopify_api\client(
                    $store_name, $TOKEN, $API_KEY, $SECRET
            );
            $chargeId = $userData[0]->charge_id;
            
            $getActivateResponse = $shopify('GET', '/admin/recurring_application_charges/' . $chargeId . '.json');                        
            if($getActivateResponse['status'] == 'pending') //|| $getActivateResponse['status'] == 'declined'
            {
				//echo '<pre>';print_r($data['data'][0]->confirmation_url);die;
                echo '<script>window.top.location.href="'.$data['data'][0]->confirmation_url.'"</script>';
                die();
            }			
            else if ($getActivateResponse['status'] == 'accepted') 
            {				
                $getAppActivate = $shopify('POST', '/admin/recurring_application_charges/' . $chargeId . '/activate.json');
                $data2['status'] = $getAppActivate['status'];
                $this->db->where(array('charge_id' => $getActivateResponse['id'], 'api_client_id' => $getActivateResponse['api_client_id']));
                $this->db->update('usersettings', $data2);
                return true;
            }
            else if ($getActivateResponse['status'] == 'active') 
            {				
                //$data2['status'] = $getActivateResponse['status'];
                //$this->db->where(array('charge_id' => $getActivateResponse['id'], 'api_client_id' => $getActivateResponse['api_client_id']));
                //$this->db->update('usersettings', $data2);
                return true;
            }
			else if ($getActivateResponse['status'] == 'declined' || $getActivateResponse['status'] == 'expired') {
				$arguments = array('recurring_application_charge' => array(
					'name' => 'multipageslider',
					'price' => 4.99,
					'return_url' => 'https://' . $this->session->userdata("shop") . '/admin/apps/multi-page-responsive-slider',
					'trial_days' => 0,
					//'test' => true,
				));

				$charge = $shopify('POST', '/admin/recurring_application_charges.json', $arguments);
				//echo '<pre>';print_r($charge);die;
				
				$data = array( 
					'charge_id' => $charge['id'],
					'api_client_id' => $charge['api_client_id'],
					'price' => $charge['price'],
					'status' => $charge['status'],
					'billing_on' => $charge['billing_on'],
					'created_at' => $charge['created_at'],
					'activated_on' => $charge['activated_on'],
					'trial_ends_on' => $charge['trial_ends_on'],
					'cancelled_on' => $charge['cancelled_on'],
					'trial_days' => $charge['trial_days'],
					'decorated_return_url' => $charge['decorated_return_url'],
					'confirmation_url' => $charge['confirmation_url'],
				);
				$this->db->where('store_name',$this->session->userdata("shop"));
                $this->db->update('usersettings', $data);
				
				echo '<script>window.top.location.href="https://'.$this->session->userdata("shop").'/admin/apps"</script>';
				die();
			}
            else
            {				
                return false;
            }

        // @TODO: Remove the following if not required
        //session_write_close();
    }

}
