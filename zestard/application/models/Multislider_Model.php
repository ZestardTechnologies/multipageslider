<?php

class Multislider_Model extends CI_Model {

   function __construct() { 
      parent::__construct(); 
   }     

   public function getStoreIdByStoreName($shop)
   {            
      $query = $this->db->get_where("usersettings",array("store_name"=>$shop));   
      return $data['shopData'] = $query->result();
   }

   public function getStoreIdByStoreNameFromController($shop)
   {                   
       $where = "store_name ='".$shop."' OR domain ='".$shop."'";
       $this->db->where($where);
       return $data['shopData'] = $this->db->get('usersettings')->result();
   }
   
   public function getCategoryData($getCryptid)
   {                  
      $query = $this->db->get_where("slider_category",array("crypt_id"=>$getCryptid));            
      return $data['categoryData'] = $query->result();
   }

   public function getItemData($id)
   {  
      $this->db->order_by("display_order", "desc");                
      $query = $this->db->get_where("slider_image_items",array("category_id"=>$id,"status"=>1));            
      return $data['records'] = $query->result();
   }

   public function getSettings($store_id)
   {                  
      $query = $this->db->get_where("slider_settings",array("store_id"=>$store_id));            
      return $data['records'] = $query->result();
   }

   public function SetData($data)
   {
      $this->GetData($data);      
   }

   public function GetData($data)
   {
      if($data != NULL) {
         echo '<pre>';print_r($data);die('aa');
      } else {
         return 0;
      }
   }
}