<?php 
class Slider_Image_Items_Model extends CI_Model {

   function __construct() { 
      parent::__construct(); 
   } 

   public function insert($data) {       
      if ($this->db->insert("slider_image_items", $data)) { 
         return true; 
      } 
   } 

   public function delete($id) { 
      if ($this->db->delete("slider_image_items", "id = ".$id)) { 
         return true; 
      } 
   } 

   public function update($data,$id) {       
      $this->db->set($data); 
      $this->db->where("id", $id); 
      $this->db->update("slider_image_items", $data); 
   }

   public function getCategoryTitleById($categoryId)
   {            
      $query = $this->db->get_where("slider_category",array("id"=>$categoryId));      
      return $data['categoryData'] = $query->result();
   }

   public function getImageById($id)
   {            
      $query = $this->db->get_where("slider_image_items",array("id"=>$id));      
      return $data['ItemImage'] = $query->result();
   }

   public function getCategories($shop)
   {            
      $shopCollection = $this->db->get_where("usersettings",array("store_name"=>$shop));            
      $getShopId = $shopCollection->result();
      $query = $this->db->get_where("slider_category",array("store_id"=>$getShopId[0]->id));      
      return $data['categoryData'] = $query->result();
   }

   public function getStoreIdByStoreName($shop)
   {            
      $query = $this->db->get_where("usersettings",array("store_name"=>$shop));      
      return $data['shopData'] = $query->result();
   } 

   public function getStoreIdByStoreNameFromController($shop)
   {                  
      $query = $this->db->get_where("usersettings",array("store_name"=>$shop));            
      return $data['shopData'] = $query->result();
   }
} 