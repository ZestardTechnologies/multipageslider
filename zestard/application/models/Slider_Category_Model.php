<?php 
class Slider_Category_Model extends CI_Model {

   function __construct() { 
      parent::__construct(); 
   } 

   public function insert($data) { 
      if ($this->db->insert("slider_category", $data)) 
      {
         $insert_id = $this->db->insert_id(); 
         $crypt_id = crypt($insert_id,'ze');
         $finaly_encrypt = str_replace(['/','.'], "Z", $crypt_id);
         $this->db->set('crypt_id',$finaly_encrypt);
         $this->db->where('id', $insert_id);
         $this->db->update('slider_category');            
         return true; 
      } 
   } 

   public function delete($id) {
      $catData = $this->getStoreIdByCryptId($id);
      $catgoryID = $catData[0]->id;
      $query = $this->db->get_where("slider_image_items",array("category_id"=>$catgoryID));      
      $data = $query->result();
      if(count($query->result()))
      {
         foreach($query->result() as $items)
         {            
            $this->db->set('category_id', 0, FALSE);
            $this->db->where('category_id', $catgoryID);
            $this->db->update('slider_image_items');
         }
      }
      if ($this->db->delete("slider_category", "id = ".$catgoryID)) { 
         return true; 
      } 
   } 

   public function update($data,$id) {   
      $this->db->set($data); 
      $this->db->where("crypt_id", $id); 
      $this->db->update("slider_category", $data); 
   } 

   public function getStoreIdByStoreName($shop)
   {            
      $query = $this->db->get_where("usersettings",array("store_name"=>$shop));      
      return $data['shopData'] = $query->result();
   }

   public function getStoreIdByStoreNameFromController($shop)
   {                  
      $query = $this->db->get_where("usersettings",array("store_name"=>$shop));            
      return $data['shopData'] = $query->result();
   }

   public function getStoreIdByCryptId($id)
   {                  
      $query = $this->db->get_where("slider_category",array("crypt_id"=>$id));            
      return $query->result();
   }
   
   public function update_modal_status($data,$shop)
   {
	  $this->db->set($data); 
      $this->db->where("store_name", $shop); 
      $this->db->update("usersettings", $data);
   }
}
