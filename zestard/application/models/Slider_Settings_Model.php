<?php
class Slider_Settings_Model extends CI_Model {

   function __construct() { 
      parent::__construct(); 
   } 

   public function insert($data) { 
      if ($this->db->insert("slider_settings", $data)) { 
         return true; 
      } 
   } 

   public function update($data,$id) {       
      $this->db->set($data); 
      $this->db->where("id", $id); 
      $this->db->update("slider_settings", $data); 
   }

   public function getStoreIdByStoreName($shop)
   {            
      $query = $this->db->get_where("usersettings",array("store_name"=>$shop));      
      return $data['shopData'] = $query->result();
   }

   public function getStoreIdByStoreNameFromController($shop)
   {                  
      $query = $this->db->get_where("usersettings",array("store_name"=>$shop));            
      return $data['shopData'] = $query->result();
   } 
}