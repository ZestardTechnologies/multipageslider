<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Help extends Base_Controller {
    
    
     public function setup() {
        if ($this->checkStatus() == true) {
            redirect('Help/index');
        } 
    }

	public function index()
	{        
		$this->load->helper('url');
		
		$this->assets = array( 
			'css' 	=> $this->config->item('css'),
			'js' 	=> $this->config->item('js'),
			'title' => 'Help Page'
    	);
    	
    	$this->page = array( 
			'page'	=>	'help'
		);

		$this->load->view('themepart/head',$this->assets);
		$this->load->view('themepart/navigation',$this->page);
		$this->load->view('help');		
	}
}