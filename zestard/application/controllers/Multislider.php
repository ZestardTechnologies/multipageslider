<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Multislider extends CI_Controller {
	
	function __construct() { 
		parent::__construct();          
		$this->load->database();
		$this->load->model('Multislider_Model');
                $this->load->library('session');
    }

	public function index()
	{		
		
		$this->assets = array( 
			'css' 	=> $this->config->item('css'),
			'js' 	=> $this->config->item('js'),
			'title' => 'Multislider'
    	);
		
		$shop = $_GET['shop'];
		$getCryptid = $_GET['multi-slider-id'];

		$getStoreId = $this->Multislider_Model->getStoreIdByStoreName($shop);  
                $this->session->set_userdata("shop",$shop);

		$query = $this->db->get_where("slider_category",array("store_id"=>$getStoreId[0]->id));
		if(!count($query->result()))
		{
			echo "<div class='custom-red-alert' style='color: #a94442;background-color: #f2dede;padding: 15px;margin-bottom: 1rem;border: 1px solid #ebcccc;border-radius: .25rem;'>
					    <strong>Alert!</strong> There are no any category! 
					</div>";
		}
		else
		{			
			$data = array();
			$categoryData = $this->Multislider_Model->getCategoryData($getCryptid);
			
			$IsCategoryEnable = $categoryData[0]->status;			

			$data['getItemData'] = $this->Multislider_Model->getItemData($categoryData[0]->id);			
			if(!count($categoryData))
			{
				echo "<div class='custom-red-alert' style='color: #a94442;background-color: #f2dede;padding: 15px;margin-bottom: 1rem;border: 1px solid #ebcccc;border-radius: .25rem;'>
					    <strong>Alert!</strong> There are no any category with this id : ".$getCryptid." 
					</div>";
			}
			else
			{
				if($IsCategoryEnable == 0)
				{
					echo "<div class='custom-red-alert' style='color: #a94442;background-color: #f2dede;padding: 15px;margin-bottom: 1rem;border: 1px solid #ebcccc;border-radius: .25rem;'>
					    <strong>Alert!</strong> Category is disabled !
					</div>";							
				}
				else
				{
					$this->load->view('frontpart/head',$this->assets);
					$this->load->view('multislider',array('getItemData' => $data['getItemData'],'store_id'=>$getStoreId[0]->id,'slider_style'=>$categoryData[0]->slider_style,'category_data'=>$categoryData));
				}				
			}			
		}							
	}
}
