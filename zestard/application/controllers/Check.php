<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Check extends CI_Controller {
	
	function __construct() { 
		parent::__construct();          
		$this->load->database();
		$this->load->model('Multislider_Model');
    }

	public function index()
	{		
		$this->assets = array( 
			'css' 	=> $this->config->item('css'),
			'js' 	=> $this->config->item('js'),
			'title' => 'Multislider'
    	);
		
		$shop = $_GET['shop'];
                $getId = $_GET['ids'];
		$getCryptids = explode(",",$getId);
		$getView = array();
		foreach($getCryptids as $getCryptid)
		{				
			ob_start();			
			$getStoreId = $this->Multislider_Model->getStoreIdByStoreNameFromController($shop);
			if($getStoreId != NULL){
				$query = $this->db->get_where("slider_category",array("crypt_id"=>$getCryptid,"store_id"=>$getStoreId[0]->id));			
				if($query->result())
				{							
					$categoryData = $query->result();				
					$IsCategoryEnable = $categoryData[0]->status;			

					$data['getItemData'] = $this->Multislider_Model->getItemData($categoryData[0]->id);			
					
					if($IsCategoryEnable == 0)
					{
						/* echo "<div class='custom-red-alert' style='color: #a94442;background-color: #f2dede;padding: 15px;margin-bottom: 1rem;border: 1px solid #ebcccc;border-radius: .25rem;'>
						    <strong>Alert!</strong> Category is disabled !
						</div>"; */
						echo "";
					}
					else
					{ 	
						$this->load->view('multisliders',array('getItemData' => $data['getItemData'],'store_id'=>$getStoreId[0]->id,'slider_style'=>$categoryData[0]->slider_style,'category_data'=>$categoryData,$this->assets));
					}				
					
				}
				else
				{
					/* echo "<div class='custom-red-alert' style='color: #a94442;background-color: #f2dede;padding: 15px;margin-bottom: 1rem;border: 1px solid #ebcccc;border-radius: .25rem;'>
						    <strong>Alert!</strong> Oops someting Wrong!
						</div>"; */
						echo "";
				}			
			}
			else
			{
				/* echo "<div class='custom-red-alert' style='color: #a94442;background-color: #f2dede;padding: 15px;margin-bottom: 1rem;border: 1px solid #ebcccc;border-radius: .25rem;'>
						<strong>Alert!</strong> Your store is wrong 
					</div>";	*/
				echo "";
			}
			$output = ob_get_contents();
			ob_end_clean();
			$getView[] = $output;			
		}		
		echo json_encode($getView); 
	}
}