<?php

//session_start();
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends Base_Controller {
    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('Slider_Category_Model');
        $this->load->library('session');
    }

    public function setup() { 
               
		if ($this->checkStatus() == true) {
            redirect('Category/index');
            
        }
    }

    public function index() {            
        $this->assets = array(
            'css' => $this->config->item('css'),
            'js' => $this->config->item('js'),
            'title' => 'Manage Category'
        );

        $this->page = array(
            'page' => 'category'
        );

        $data = $this->Slider_Category_Model->getStoreIdByStoreName($this->session->userdata['shop']);

        $query = $this->db->get_where("slider_category", array("store_id" => $data[0]->id));		
        $data['records'] = $query->result();                
        $query = $this->db->get_where("usersettings", array("id" => $data[0]->id));		
		$user_settings = $query->result();		
		//if($this->session->userdata['shop'] == "zestardgiftshop.myshopify.com")
		{
			$data['new_install'] = $user_settings[0]->new_install;			
		}			

        //session_write_close();

        $this->load->view('themepart/head', $this->assets);
        $this->load->view('themepart/navigation', $this->page);
        $this->load->view('categorylist', $data);
    }
	
	public function update_modal_status()
	{
		$shop = $_GET['shop_name'];
		//For new installation modal status
		$this->Slider_Category_Model->update_modal_status(['new_install' => 'N'], $shop);			
		//DB::table('usersettings')->where('store_name' , $shop)->update(['new_install' => 'N']);				
	}

    public function delete() {
        $this->load->model('Slider_Category_Model');
        $id = $this->uri->segment('3');
        $this->Slider_Category_Model->delete($id);

        redirect('/category/');
    }

    public function edit() {
        $this->assets = array(
            'css' => $this->config->item('css'),
            'js' => $this->config->item('js'),
            'title' => 'Manage Category'
        );

        $this->page = array(
            'page' => 'category'
        );

        $this->load->helper('form');
        $id = $this->uri->segment('3');
        $query = $this->db->get_where("slider_category", array("crypt_id" => $id));
        $data['records'] = $query->result();
        $data['id'] = $id;

        $this->load->view('themepart/head', $this->assets);
        $this->load->view('themepart/navigation', $this->page);
        if (empty($data['records']))
            $this->load->view('404');
        else
            $this->load->view('category_edit', $data);
    }

    public function category_update() {
        $this->load->model('Slider_Category_Model');
        $data = array(
            'title' => $this->input->post('title'),
            'status' => $this->input->post('status'),
            'slider_style' => $this->input->post('slider_style'),
            'catgory_slide_time' => $this->input->post('catgory_slide_time'),
            'catgory_class_name' => $this->input->post('catgory_class_name'),
            'catgory_inline_css' => $this->input->post('catgory_inline_css'),
            'item_effects' => $this->input->post('item_effects')
        );

        $id = $this->input->post('id');
        $this->Slider_Category_Model->update($data, $id);

        redirect('/category/');
        echo "<div class='alert alert-success'>
			Your category has been updated.
		</div>";
    }

    public function add() {
        $this->assets = array(
            'css' => $this->config->item('css'),
            'js' => $this->config->item('js'),
            'title' => 'Manage Category'
        );

        $this->page = array(
            'page' => 'category'
        );

        $this->load->helper('form');

        $this->load->view('themepart/head', $this->assets);
        $this->load->view('themepart/navigation', $this->page);
        $this->load->view('category_add');
    }

    public function category_add() {

$store_data = $this->Slider_Category_Model->getStoreIdByStoreNameFromController($this->session->userdata['shop']);


/* if( $_SERVER['REMOTE_ADDR'] == "49.36.1.6112") //12.205.204.254
{
print_r("<pre>");
print_r($_POST);

print_r($store_data['0']->id);
exit;
} */

        $this->load->model('Slider_Category_Model');
        $data = array(
            'title' => $this->input->post('title'),
            'status' => $this->input->post('status'),
            'slider_style' => $this->input->post('slider_style'),
            'catgory_slide_time' => $this->input->post('catgory_slide_time'),
            'catgory_class_name' => $this->input->post('catgory_class_name'),
            'catgory_inline_css' => $this->input->post('catgory_inline_css'),
            'item_effects' => $this->input->post('item_effects'),
            'store_id' => $store_data['0']->id,
        );

        $this->Slider_Category_Model->insert($data);
        redirect('/category/');
        echo "<div class='alert alert-success'>
			Your category has been inserted.
		</div>";
    }

}
