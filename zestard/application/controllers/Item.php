<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Item extends Base_Controller {
	
	function __construct() { 
		parent::__construct();          
		$this->load->database(); 
		$this->load->model('Slider_Image_Items_Model');
                $this->load->library("session");
    } 
    public function setup() {
        if ($this->checkStatus() == true) {
            redirect('Item/index');
        }
    }
	public function index()
	{        
        	$this->assets = array( 
			'css' 	=> $this->config->item('css'),
			'js' 	=> $this->config->item('js'),
			'title' => 'Manage Gallery Items'
    	);
    	
    	$this->page = array( 
			'page'	=>	'item'
		);
    	
    	$shopCollection = $this->Slider_Image_Items_Model->getStoreIdByStoreName($this->session->userdata("shop"));

    	$query = $this->db->get_where("slider_image_items",array("store_id"=>$shopCollection[0]->id));    			
        $data['records'] = $query->result(); 			        			

		$this->load->view('themepart/head',$this->assets);
		$this->load->view('themepart/navigation',$this->page);
		$this->load->view('itemslist',$data);		
	}

	public function delete() 
	{
		$this->load->model('Slider_Image_Items_Model'); 
		$id = $this->uri->segment('3');		
		$this->Slider_Image_Items_Model->delete($id); 		
		redirect('/item/');
    }

    public function edit()
    { 	
    	$this->assets = array( 
			'css' 	=> $this->config->item('css'),
			'js' 	=> $this->config->item('js'),
			'title' => 'Manage Items'
    	);
    	
    	$this->page = array( 
			'page'	=>	'item'
		);

		$storeID = $this->Slider_Image_Items_Model->getStoreIdByStoreNameFromController($_SESSION['shop']);

		$this->load->helper('form'); 
		$id = $this->uri->segment('3'); 
		$query = $this->db->get_where("slider_image_items",array("id"=>$id,"store_id"=>$storeID[0]->id));
		$data['records'] = $query->result();
		$data['id'] = $id;

		$this->load->view('themepart/head',$this->assets);
		$this->load->view('themepart/navigation',$this->page);
		if(empty($data['records']))
			$this->load->view('404');
		else
			$this->load->view('item_edit',$data);
		
    }

    public function add() 
    { 		
		$this->assets = array( 
			'css' 	=> $this->config->item('css'),
			'js' 	=> $this->config->item('js'),
			'title' => 'Manage Gallery Items'
		);

		$this->page = array( 
			'page'	=>	'item'
		);
	
		$this->load->helper('form'); 
		
		$this->load->view('themepart/head',$this->assets);
		$this->load->view('themepart/navigation',$this->page);
		$this->load->view('item_add');		
    }

    public function item_add()
    {				
		
		$this->assets = array( 
			'css' 	=> $this->config->item('css'),
			'js' 	=> $this->config->item('js'),
			'title' => 'Manage Gallery Items'
		);

		$this->page = array( 
			'page'	=>	'item'
		);

		$config = array();
		$config['upload_path'] 		= './assets/uploads/'.$this->input->post('store_id').'/';
		$config['allowed_types'] 	= 'gif|jpg|png|jpeg';
		$config['max_size']			= '2048';
		$config['overwrite']     	= FALSE;

		if(!is_dir($config['upload_path']))
			mkdir($config['upload_path'], 0777, true);

		$this->load->model('Slider_Image_Items_Model');		

		$data = array( 
			'store_id' 		=> $this->input->post('store_id'),
			'title' 		=> $this->input->post('title'), 
			'category_id' 	=> $this->input->post('category_id'), 
			'shopnow_title' => $this->input->post('shopnow_title'), 
			'shopnow_url' 	=> $this->input->post('shopnow_url'),
			'item_show_shopnow' => $this->input->post('item_show_shopnow'),
			'item_show_title' 	=> $this->input->post('item_show_title'),
			'item_target_window'=> $this->input->post('item_target_window'), 
			'image_content' => $this->input->post('image_content'),
			'image' 		=> $_FILES['image']['name'],
                        'image_link_status' => $this->input->post('image_link_status'),
                        'image_link_url' => $this->input->post('image_url'),
                        'banner_target_window'=> $this->input->post('banner_target_window'), 
			'status' 		=> $this->input->post('status'),
                        'display_order' => $this->input->post('display_order')
		);
                //print_r($data);die();
		//load upload class library
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        
        if ($this->upload->do_upload('image'))
        {
			$uploadedImageData = $this->upload->data();
			$imageName = $uploadedImageData['raw_name'].$uploadedImageData['file_ext'];
			$data['image'] = $imageName;
            $this->Slider_Image_Items_Model->insert($data);
            $sucess = array('success' => $this->upload->data(),'success_msg' => 'Your item has been saved.');            
            $this->load->view('themepart/head',$this->assets);
			$this->load->view('themepart/navigation',$this->page);
			$this->load->view('item_add',$sucess);			           
        }
        else
        {            
        	$error = array('error' => $this->upload->display_errors());
            $this->load->view('themepart/head',$this->assets);
			$this->load->view('themepart/navigation',$this->page);
			$this->load->view('item_add',$error);			      
        }
				       
    }

    public function item_update()
    {      	
     	$id = $this->input->post('id');
     	$this->load->model('Slider_Image_Items_Model');
     	$imageCollection = $this->Slider_Image_Items_Model->getImageById($id);		
		$image = '';     	
     	if($_FILES['image']['name'])
     		$image = $_FILES['image']['name'];
     	else 
     		$image = $imageCollection[0]->image;

    	$data = array( 
            'title' 		=> $this->input->post('title'), 
            'category_id' 	=> $this->input->post('category_id'), 
            'shopnow_title' => $this->input->post('shopnow_title'), 
            'shopnow_url' 	=> $this->input->post('shopnow_url'), 
            'item_show_shopnow' => $this->input->post('item_show_shopnow'),
            'item_show_title' 	=> $this->input->post('item_show_title'),
            'item_target_window'=> $this->input->post('item_target_window'), 
            'image_content' => $this->input->post('image_content'),
            'image_link_status' => $this->input->post('image_link_status'),
            'image_link_url' => $this->input->post('image_url'),
            'banner_target_window'=> $this->input->post('banner_target_window'),
            'status' 		=> $this->input->post('status'),
            'display_order' => $this->input->post('display_order')
        );
		
        $config = array();
		$config['upload_path'] 		= './assets/uploads/'.$this->input->post('store_id').'/';
		$config['allowed_types'] 	= 'gif|jpg|png';
		$config['max_size']			= '2048';
		$config['overwrite']     	= FALSE;

		if(!is_dir($config['upload_path'])){
			mkdir($config['upload_path'], 0777, true);
		}	
		$this->load->library('upload', $config);
        $this->upload->initialize($config);

        if($_FILES['image']['name'])
        {       

        	if ($this->upload->do_upload('image'))
	        {
				$uploadedImageData = $this->upload->data();
				$imageName = $uploadedImageData['raw_name'].$uploadedImageData['file_ext'];
				$data['image'] = $imageName;				
	            $this->Slider_Image_Items_Model->update($data,$id);	            
	            redirect('/item/');
	        }
	        else
	        {
	        	redirect('/item/');
	        }
        }
        else
        {
        	if($image == $imageCollection[0]->image)
	        {
	        	$this->Slider_Image_Items_Model->update($data,$id);	            
	            redirect('/item/');
	        }
	        else
	        {            
	        	$error = array('error' => $this->upload->display_errors());	        	
	        	redirect('/item/');
	        }
        }                     		        
    }
}
