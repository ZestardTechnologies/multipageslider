<?php
//session_start();
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends Base_Controller {
	
	function __construct() { 
		parent::__construct();          
		$this->load->database(); 
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->model('Slider_Settings_Model');
                $this->load->library('session');
    } 

        public function setup() {
        if ($this->checkStatus() == true) {
            redirect('Category/index');
        }
    }
	public function index()
	{	
              $this->setup();
		
		$this->assets = array( 
			'css' 	=> $this->config->item('css'),
			'js' 	=> $this->config->item('js'),
			'title' => 'Manage Settings'
    	);
    	
    	$this->page = array( 
			'page'	=>	'settings'
		);
    	
    	$data = $this->Slider_Settings_Model->getStoreIdByStoreName($this->session->userdata['shop']);    	
    	$query = $this->db->get_where("slider_settings",array("store_id"=>$data[0]->id));
		$data['records'] = $query->result();

		//session_write_close();
		
		$this->load->view('themepart/head',$this->assets);
		$this->load->view('themepart/navigation',$this->page);
		$this->load->view('settings',$data);		
	}

	public function add()
	{		
		$this->load->model('Slider_Settings_Model');			
    	$data = array( 
            'store_id' 		=> $this->input->post('store_id'), 
            'slide_time' 	=> $this->input->post('slide_time'), 
            'show_shopnow' 	=> $this->input->post('show_shopnow'), 
            'show_title' 	=> $this->input->post('show_title'), 
            'target_window' => $this->input->post('target_window') 
        );
        $id = $this->input->post('id');

        if($id == NULL)         			        
        	$this->Slider_Settings_Model->insert($data); 
        else
        	$this->Slider_Settings_Model->update($data,$id); 
		
        redirect('/settings/');
	}
}
