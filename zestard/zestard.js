var base_path_multipage_slider = "https://www.zestardshop.com/shopifyapp/multipageslider/zestard/";
var urls = [base_path_multipage_slider + "assets/js/jquery.flexslider.js"];
for (var i = 0; i < urls.length; i++) {
    var s = document.createElement('script');
    s.type = 'text/javascript';
    s.async = false;
    s.src = urls[i];
    var x = document.getElementsByTagName('script')[0];
    x.parentNode.insertBefore(s, x);
}

window.attachEvent ? window.attachEvent('onload', asyncLoad) : window.addEventListener('load', asyncLoad, false);

function asyncLoad() {

    jQuery('.slider').each(function() {
        var slideTime = jQuery(this).find('.cat_slide_time').val();
        if (Shopify.shop == "the-london-beauty-company.myshopify.com") {
            console.log('in Async');
            jQuery(this).find('.zestard_multipage_flexslider').flexslider({
                //jQuery(this).find('.zestard_multipage_flexslider').flexslider({
                animation: "slide",
                slideshowSpeed: slideTime,
                controlNav: false,
                //itemWidth: 1000,
                start: function(slider) {
                    jQuery('body').removeClass('loading');
                }
            });
        } else {
            jQuery(this).find('.zestard_multipage_flexslider').flexslider({
                //jQuery(this).find('.zestard_multipage_flexslider').flexslider({
                animation: "slide",
                slideshowSpeed: slideTime,
                //itemWidth: 1000,
                start: function(slider) {
                    jQuery('body').removeClass('loading');
                }
            });
        }
    });


    jQuery('.cslide li').click(function() {
        var val = parseInt(jQuery(this).index() + 1);
        jQuery(this).parent().parent().find('.flex-control-nav li:nth-child(' + val + ') a').click();

    });
}
(function() {
    // Your base, I'm in it!
    var originalAddClassMethod = jQuery.fn.addClass;
    jQuery.fn.addClass = function() {
        // Execute the original method.
        var result = originalAddClassMethod.apply(this, arguments);
        // call your function
        // this gets called everytime you use the addClass method
        window.checkFunction(result);
        // return the original result
        return result;
    }
})();

function checkFunction(result) {
    if (result.hasClass('flex-active')) {
        var indexNumber = parseInt(result.parent().index()) + 1;
        jQuery(result).parents('.flexslider').find('.cslide li a').removeClass('flex-active');
        jQuery(result).parents('.flexslider').find('.cslide li:nth-child(' + indexNumber + ')').find('a').attr('class', 'flex-active');
    }
}

loadCSS = function(href) {
    var cssLink = $("<link rel='stylesheet' type='text/css' href='" + href + "'>");
    jQuery("head").append(cssLink);
};
loadCSS(base_path_multipage_slider + "assets/css/flexslider.css");
loadCSS(base_path_multipage_slider + "assets/css/custom.css");

jQuery(document).ready(function() {
    var ids = jQuery('.zestard-multislider').map(function() {
        return this.id
    }).get();
    var shop = Shopify.shop;
    jQuery.ajax({
        url: base_path_multipage_slider + 'check?ids=' + ids + '&shop=' + shop,
        dataType: "json",
        method: "get",
        success: function(res) {
            var count = 0;
            jQuery(".zestard-multislider").each(function() {
                var that = this;
                jQuery(this).html(res[count]);
                count++;
            });
            asyncLoad();
        },
        error: function(xhr, status, err) {
            jQuery("<p>Error: Status = " + status + ", err = " + err + "</p>")
                .appendTo(document.body);
        }
    });
    if (Shopify.shop == "the-london-beauty-company.myshopify.com") {
        console.log('here');
        console.log($(".flex-control-nav").css("display", "none !important"));
    }
});