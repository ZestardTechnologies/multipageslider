var Gallery = Class.create({
  initialize: function() {
    this.initSelf(this);
  },

  initSelf:function(selfReference)
  {
    this.selfReference = selfReference;
  },

  initslider: function(slidetime)
  {
    jQuery('.flexslider').flexslider({
        animation: "slide",
        slideshowSpeed: slidetime,
        start: function(slider)
        {
          jQuery('body').removeClass('loading');
        }
    });

  jQuery(document).on('click','.readmoreLink',function(){
    jQuery(this).hide();
    jQuery(this).prev().show();
      jQuery(this).prev().css('display','inline');
  });
  // Create a closure
  (function(){
  // Your base, I'm in it!
  var originalAddClassMethod = jQuery.fn.addClass;
    jQuery.fn.addClass = function(){
      // Execute the original method.
      var result = originalAddClassMethod.apply( this, arguments );
      // call your function
      // this gets called everytime you use the addClass method
      window.checkFunction(result);
      // return the original result
      return result;
    }
  })();
   }
});

function checkFunction(result)
{
  if(result.hasClass('flex-active'))
  {
    var indexNumber = parseInt(result.parent().index())+1;
    jQuery(result).parents('.flexslider').find('.cslide li a').removeClass('flex-active');
    jQuery(result).parents('.flexslider').find('.cslide li:nth-child('+indexNumber+')').find('a').attr('class','flex-active');
  }
}