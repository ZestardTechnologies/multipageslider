<?php

session_start(); //start a session
require 'vendor/autoload.php';
use shopifymodel\shopify_api;

$db = new Mysqli("localhost", "zestards_shopify", "pi#gyHnppiJH", "zestards_shopifylive_multipage_slider");

if($db->connect_errno){
  die('Connect Error: ' . $db->connect_errno);
}

$select_settings = $db->query("SELECT * FROM appsettings WHERE id = 1");
$app_settings = $select_settings->fetch_object();

if(!empty($_GET['shop']) && !empty($_GET['code']))
{

  $shop = $_GET['shop']; //shop name

  //get permanent access token
  $access_token = shopify_api\oauth_access_token(
      $_GET['shop'], $app_settings->api_key, $app_settings->shared_secret, $_GET['code']
  );
  $shopify = shopify_api\client(
                    $shop, $access_token, $app_settings->api_key, $app_settings->shared_secret
    );
	
	//get theme data
	$argumentsTheme = array('role'=>'main');
	$themeFiles = $shopify('GET', '/admin/themes.json',$argumentsTheme);
	
	//add scriptag
	$scriptData = array(
		'script_tag' => array(
			'event' => 'onload', 
			'src' 	=> 'https://zestardshop.com/shopifyapp/multipageslider/zestard/zestard.js'
		)
	);
	$scriptTag = $shopify('POST', '/admin/script_tags.json',$scriptData);
	
	if($shop == "shopify-dev-app.myshopify.com")
	{
		$arguments = array('recurring_application_charge' => array(
			'name' => 'multipageslider',
			'price' => 4.99,
			'return_url' => 'https://' . $shop . '/admin/apps/multi-page-responsive-slider',
			'trial_days' => 0,
			'test' => true,
		));
	}
	else		
	{
		$arguments = array('recurring_application_charge' => array(
			'name' => 'multipageslider',
			'price' => 4.99,
			'return_url' => 'https://' . $shop . '/admin/apps/multi-page-responsive-slider',
			'trial_days' => 0,
			//'test' => true,
		));
	}

    $charge = $shopify('POST', '/admin/recurring_application_charges.json', $arguments);
	//webhook for unistall app  
       
  $url = 'https://'.$_GET['shop'].'/admin/webhooks.json';
  
  $params = array("webhook" => array( "topic"=>"app/uninstalled",
    "address"=> "https://www.zestardshop.com/shopifyapp/multipageslider/hook.php",
    "format"=> "json"));
  $appUninstallHook = shopify_api\appUninstallHook(
      $access_token, $url, $params
  );
  
  $shopifydomain = shopify_api\client(
                   $_GET['shop'], $access_token,$app_settings->api_key, $app_settings->shared_secret
    );
    $getshopresponse = array();
    $getshopresponse = $shopifydomain('GET', '/admin/shop.json');
    $domain = $getshopresponse['domain'];
  //save the shop details to the database
  $db->query('INSERT INTO usersettings (access_token, store_name, charge_id, api_client_id, price, status, billing_on, created_at, activated_on, trial_ends_on, cancelled_on, trial_days, decorated_return_url, confirmation_url,domain)
VALUES (
  "' . $access_token . '",
  "' . $shop . '",
  "' . $charge["id"] . '",
  "' . $charge["api_client_id"] . '",
  "' . $charge["price"] . '",
  "' . $charge["status"] . '",
  "' . $charge["billing_on"] . '",
  "' . $charge["created_at"] . '",
  "' . $charge["activated_on"] . '",
  "' . $charge["trial_ends_on"] . '",
  "' . $charge["cancelled_on"] . '",
  "' . $charge["trial_days"] . '",
  "' . $charge["decorated_return_url"] . '",
  "' . $charge["confirmation_url"] . '",
  "' . $shop . '"
          
  )'
    );

    $headers = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

    $msg = '<table>
                            <tr>
                                <th>Shop Name</th>
                                <td>' . $getshopresponse['name'] . '</td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td>' . $getshopresponse['email'] . '</td>
                            </tr>
                            <tr>
                                <th>Domain</th>
                                <td>' . $getshopresponse['domain'] . '</td>
                            </tr>
                            <tr>
                                <th>Phone</th>
                                <td>' . $getshopresponse['phone'] . '</td>
                            </tr>
                            <tr>
                                <th>Shop Owner</th>
                                <td>' . $getshopresponse['shop_owner'] . '</td>
                            </tr>
                            <tr>
                                <th>Country</th>
                                <td>' . $getshopresponse['country_name'] . '</td>
                            </tr>
                            <tr>
                                <th>Plan</th>
                                <td>' . $getshopresponse['plan_name'] . '</td>
                            </tr>
                          </table>';

    mail("support@zestard.com", "Multi Page Responsive Slider App Installed", $msg, $headers);   
    
  
  //save the signature and shop name to the current session
  $_SESSION['shopify_signature'] = $_GET['signature'];
  $_SESSION['shop'] = $shop;

//echo $charge["confirmation_url"];
  //header('Location: https://www.zestardshop.com/shopifyapp/multipageslider/zestard/');
   header('Location: ' . $charge["confirmation_url"] . '');
}
?>